Payout Frontend
================================
Version:  1.0.0
Date:  04/02/2015
Contributors:  Tim Mahler, Ryan Templin, Nick Burrin, Reed Cozart

********** Description *********
- Android app for payout

************ Config ************
- Android studio
- Java, XML, standard Android dev tools & packages

************* Files ************
