package com.example.ryantemplin.payoutapp;

import android.app.Instrumentation;
import android.test.ActivityInstrumentationTestCase2;
import android.test.InstrumentationTestCase;

import com.example.ryantemplin.payoutapp.activities.BetActivity;

/**
 * Created by ryantemplin on 4/14/15.
 */
public class BetsActivityTest extends ActivityInstrumentationTestCase2<BetActivity> {

    public BetsActivityTest() {
        super(BetActivity.class);
    }

    // This test the scenario if the user is in the bets page of PayOut and the user clicks on the games button
    // The test makes sure that the user is taken to the games activity
    public void testGamesButton(){
    }

    // This test the scenario if the user is in the games page of PayOut and the user clicks on the Account button
    // The test makes sure that the user is taken to the account activity
    public void testAccountButton(){

    }

    //This test the scenario if the user selects a particular game to place an individual bet
    //If the user presses on this particular button, the type of bets the user can look at old games bets results
    public void testPlaceBetButton(){

    }
}
