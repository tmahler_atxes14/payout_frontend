package com.example.ryantemplin.payoutapp;

import android.app.Instrumentation;
import android.content.Intent;
import android.media.audiofx.AutomaticGainControl;
import android.test.ActivityInstrumentationTestCase2;
import android.test.UiThreadTest;
import android.text.Editable;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TextView;

import com.example.ryantemplin.payoutapp.activities.LoginActivity;
import com.example.ryantemplin.payoutapp.activities.RegisterUserActivity;
import com.example.ryantemplin.payoutapp.asyncTask.LoginTask;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


/**
 * Created by nickburrin on 4/14/15.
 */
public class LoginActivityTest extends ActivityInstrumentationTestCase2<LoginActivity> {

    private LoginActivity testActivity;
    private Instrumentation mInstrumentation;



    public LoginActivityTest() {
        super(LoginActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        testActivity = getActivity();
        System.out.println("Inside Setup()");
        mInstrumentation = getInstrumentation();
    }

//    @Override
//    protected void tearDown() throws Exception{
//
//    }
    /*
        This method will test that the box for inputting email will accept input.
     */
    public void testEmailBox(){
        testActivity = getActivity();
        final AutoCompleteTextView mEmailView = (AutoCompleteTextView) testActivity.findViewById(R.id.et_email);
        Editable actual, expected;
        testActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mEmailView.setText("a.email@domain.com", AutoCompleteTextView.BufferType.EDITABLE);
            }
        });
        mInstrumentation.waitForIdleSync();
        actual = mEmailView.getText();
        expected = new Editable.Factory().newEditable("a.email@domain.com");
        assertEquals(actual.toString(), expected.toString());
    }

    /*
        This method will test that the box for inputting a password will accept input.
     */

    public void testEmailpasswordBox(){
        testActivity = getActivity();
        System.out.print("TESTING PASSWORD BOX");
        final EditText mPasswordView = (EditText) testActivity.findViewById(R.id.et_password);
        Editable actual, expected;

        testActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mPasswordView.setText("apassword", EditText.BufferType.EDITABLE);
            }
        });
        mInstrumentation.waitForIdleSync();
        actual = mPasswordView.getText();
        expected = new Editable.Factory().newEditable("apassword");
        assertEquals(actual.toString(), expected.toString());
       // assertTrue(true);
    }

    //public void testPasswordBox2(){
        /*final EditText mPasswordView = (EditText) testActivity.findViewById(R.id.et_password);
        Editable actual, expected;
        testActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mPasswordView.setText("apassword", EditText.BufferType.EDITABLE);
            }
        });
        mInstrumentation.waitForIdleSync();
        actual = mPasswordView.getText();
        expected = new Editable.Factory().newEditable("apassword");
        assertEquals(actual.toString(), expected.toString()); */
    //}

    /*
        This method will test whether the Login button functions correctly.
        @Preconditions: both email and password TextEdit contain values
        @Postconditions:
        -check error conditions for login
     */
    public void testLoginButton(){
        testActivity = getActivity();
        final AutoCompleteTextView mEmailView = (AutoCompleteTextView) testActivity.findViewById(R.id.et_email);
        final EditText mPasswordView = (EditText) testActivity.findViewById(R.id.et_password);
        testActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mEmailView.setText("reed.cozart@gmail.com", AutoCompleteTextView.BufferType.EDITABLE);
                mPasswordView.setText("pa", EditText.BufferType.EDITABLE);
                testActivity.attemptLogin();
            }
        });
        getInstrumentation().waitForIdleSync();
        assertEquals("This password is too short", mPasswordView.getError());

        testActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mEmailView.setText("", AutoCompleteTextView.BufferType.EDITABLE);
                mPasswordView.setText("password", EditText.BufferType.EDITABLE);
                testActivity.attemptLogin();
            }
        });
        getInstrumentation().waitForIdleSync();
        assertEquals("This field is required", mEmailView.getError());

        testActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mEmailView.setText("reed.cozartgmail.com", AutoCompleteTextView.BufferType.EDITABLE);
                mPasswordView.setText("password", EditText.BufferType.EDITABLE);
                testActivity.attemptLogin();
            }
        });
        getInstrumentation().waitForIdleSync();
        assertEquals("This email address is invalid", mEmailView.getError());
    }

    /*
        This button should take the user to the RegisterUserActivity, regardless of state
     */
    public void testNewRegisterButton(){
        Instrumentation.ActivityMonitor registerUserActivityMonitor =
                getInstrumentation().addMonitor(RegisterUserActivity.class.getName(),
                        null, false);
        testActivity.attemptRegister();
        RegisterUserActivity registerUserActivity = (RegisterUserActivity)
                registerUserActivityMonitor.waitForActivityWithTimeout(1000);
        assertNotNull("RegisterUserActivity is null", registerUserActivity);
        assertEquals("Monitor for ReceiverActivity has not been called",
                1, registerUserActivityMonitor.getHits());
        assertEquals("Activity is of wrong type",
                RegisterUserActivity.class, registerUserActivity.getClass());

        getInstrumentation().removeMonitor(registerUserActivityMonitor);


    }
}
