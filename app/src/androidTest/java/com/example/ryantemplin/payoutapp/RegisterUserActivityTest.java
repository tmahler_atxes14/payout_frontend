package com.example.ryantemplin.payoutapp;

import android.app.Instrumentation;
import android.test.ActivityInstrumentationTestCase2;
import android.test.InstrumentationTestCase;
import android.text.Editable;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import com.example.ryantemplin.payoutapp.activities.LoginActivity;
import com.example.ryantemplin.payoutapp.activities.RegisterUserActivity;

/**
 * Created by nickburrin on 4/14/15.
 */
public class RegisterUserActivityTest extends ActivityInstrumentationTestCase2<RegisterUserActivity> {

    public RegisterUserActivityTest() {
        super(RegisterUserActivity.class);
    }


    public RegisterUserActivity testActivity;
    private Instrumentation mInstrumentation;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        testActivity = getActivity();
        System.out.println("Inside Setup()");
        mInstrumentation = getInstrumentation();
    }

    /*
        This method will test that the box for inputting first name will accept input.
     */
    public void testFirstNameBox(){
    final EditText firstName = (EditText) testActivity.findViewById(R.id.et_first_name);
    Editable actual, expected;
    testActivity.runOnUiThread(new Runnable() {
        @Override
        public void run() {
            firstName.setText("Reed", AutoCompleteTextView.BufferType.EDITABLE);
        }
    });
    getInstrumentation().waitForIdleSync();
    actual = firstName.getText();
    expected = new Editable.Factory().newEditable("Reed");
    assertEquals("actual and expected are not the same",actual.toString(), expected.toString());
    }
    /*
        This method will test that the box for inputting last name will accept input.
     */
    public void testLastNameBox(){
        final EditText lastName = (EditText) testActivity.findViewById(R.id.et_last_name);
        Editable actual, expected;
        testActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                lastName.setText("Cozart", EditText.BufferType.EDITABLE);
            }
        });
        mInstrumentation.waitForIdleSync();
        actual = lastName.getText();
        expected = new Editable.Factory().newEditable("Cozart");
        assertEquals(actual.toString(), expected.toString());
    }
    /*
        This method will test that the box for inputting email will accept input.
     */
    public void testEmailBox(){
        final AutoCompleteTextView mEmailView = (AutoCompleteTextView) testActivity.findViewById(R.id.et_email);
        Editable actual, expected;
        testActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mEmailView.setText("a.email@domain.com", AutoCompleteTextView.BufferType.EDITABLE);
            }
        });
        mInstrumentation.waitForIdleSync();
        actual = mEmailView.getText();
        expected = new Editable.Factory().newEditable("a.email@domain.com");
        assertEquals(actual.toString(), expected.toString());
    }

    /*
        This method will test that the box for inputting a password will accept input.
     */
    public void testPasswordBox(){
        final EditText mPasswordView = (EditText) testActivity.findViewById(R.id.et_password);
        Editable actual, expected;
        testActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mPasswordView.setText("apassword", EditText.BufferType.EDITABLE);
            }
        });
        mInstrumentation.waitForIdleSync();
        actual = mPasswordView.getText();
        expected = new Editable.Factory().newEditable("apassword");
        assertEquals(actual.toString(), expected.toString());
    }

    /*
        This method will test whether the register button functions correctly.
        @Preconditions: both first name, last name, email, password TextEdit contain values
        @Postconditions:
        - if the first name, last name, email, and password are valid, then a asynchronous
        task is spawned and submits this to the backend for creating an account.
        - if the any of the fields are empty, then an error is shown
     */
    public void testRegisterButton(){}

    /*
        This button should take the user to the LoginActivity, regardless of state
     */


    public void testExistingUserButton(){
        Instrumentation.ActivityMonitor loginActivityMonitor =
                getInstrumentation().addMonitor(LoginActivity.class.getName(),
                        null, false);
        final Button existingUserButton = (Button) testActivity.findViewById(R.id.button_existing_user);
        testActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                existingUserButton.performClick();
            }
        });
        getInstrumentation().waitForIdleSync();
        LoginActivity loginActivity = (LoginActivity)
                loginActivityMonitor.waitForActivityWithTimeout(10000);

        assertNotNull("LoginActivity is null", loginActivity);
        assertEquals("Monitor for LoginActivity has not been called",
                1, loginActivityMonitor.getHits());
        assertEquals("Activity is of wrong type",
                LoginActivity.class, loginActivity.getClass());

        getInstrumentation().removeMonitor(loginActivityMonitor);
    }
}
