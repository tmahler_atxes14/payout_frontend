package com.example.ryantemplin.payoutapp;

import android.app.Instrumentation;
import android.content.Intent;
import android.test.ActivityInstrumentationTestCase2;
import android.test.ActivityUnitTestCase;

import com.example.ryantemplin.payoutapp.activities.LoginActivity;
import com.example.ryantemplin.payoutapp.activities.RegisterUserActivity;
import com.example.ryantemplin.payoutapp.activities.SplashActivity;

/**
 * Created by Havoc on 5/5/15.
 */
public class SplashActivityTest extends ActivityUnitTestCase<SplashActivity> {
    public SplashActivityTest() {
        super(SplashActivity.class);
    }

    public void testSplashActivityLaunches() {
        SplashActivity mActivity = startActivity(new Intent(), null, null);
        getInstrumentation().waitForIdle(new Runnable() {
            public void run() {
                assertNotNull(getStartedActivityIntent());
                assertTrue(isFinishCalled());
            }
        });
    }

}
