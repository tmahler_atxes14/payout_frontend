package com.example.ryantemplin.payoutapp.endpoints;

import android.test.ActivityInstrumentationTestCase2;

import com.example.ryantemplin.payoutapp.activities.RegisterUserActivity;
import com.example.ryantemplin.payoutapp.asyncTask.DeleteUserTask;
import com.example.ryantemplin.payoutapp.asyncTask.LoginTask;
import com.example.ryantemplin.payoutapp.asyncTask.RegisterUserTask;
import com.example.ryantemplin.payoutapp.util.Logger;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.util.concurrent.ExecutionException;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by nickburrin on 5/9/15.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserTest extends ActivityInstrumentationTestCase2<RegisterUserActivity>
        implements RegisterUserTask.OnRegisterListener,
        DeleteUserTask.OnDeleteUserListener,
        LoginTask.OnLoginListener{

    String TAG = UserTest.class.getSimpleName();

    private RegisterUserActivity mRegisterUserTestActivity;
    private int userId;
    private final String email;
    private final String password;
    private boolean didDelete;

    public UserTest() {
        super(RegisterUserActivity.class);
        email = "joe.blow@example.net";
        password = "password123";
        userId = -1;
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mRegisterUserTestActivity = getActivity();
    }

    @Test
    public void test1_RegisterUser(){

        RegisterUserTask register = new RegisterUserTask("joe", "blow", email, password, this);
        register.execute();
        String result = null;
        Logger.d(TAG, "instantiated register user task");

        try {
            result = register.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        JSONObject resultJSON = null;
        try {
            resultJSON = new JSONObject(result);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            userId = resultJSON.getInt("id");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        assertTrue(userId != -1);
    }

    @Test
    public void test2_UserLogin(){
        LoginTask login = new LoginTask(email, password, this);
        login.execute();

        String result = null;
        JSONObject resultJSON = null;
        Logger.d(TAG, "instantiated login task");

        try {
            result = login.get();
            resultJSON = new JSONObject(result);
            userId = resultJSON.getInt("id");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        assertTrue(userId != -1);
    }

    @Test
    public void test3_DeleteUser(){

        LoginTask login = new LoginTask(email, password, this);
        login.execute();

        String result = null;
        JSONObject resultJSON = null;
        Logger.d(TAG, "instantiated login task");

        try {
            result = login.get();
            resultJSON = new JSONObject(result);
            userId = resultJSON.getInt("id");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        DeleteUserTask delete = new DeleteUserTask(((Integer)userId).toString(), this, null);
        delete.execute();

        String deleteResult = null;
        JSONObject deleteResultJSON = null;
        try {
            deleteResult = delete.get();
            deleteResultJSON = new JSONObject(deleteResult);
            deleteResult = deleteResultJSON.getString("msg");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        assertEquals(deleteResult, "Successfully deleted users");
    }

    @Override
    public void onRegisterSuccess(int id) {
        Logger.d(TAG, "success registering user with id:" + id);
    }

    @Override
    public void onRegisterError(String message) {
        Logger.d(TAG, "error registering user: " + message);
    }

    @Override
    public void OnDeleteUserSuccess() {
        Logger.d(TAG, "success deleting user");
    }

    @Override
    public void OnDeleteUserError(String message) {
        Logger.d(TAG, "error deleting user: " + message);
    }

    @Override
    public void onLoginSuccess(int id) {
        Logger.d(TAG, "user login success, id=" + id);
    }

    @Override
    public void onLoginError(String message) {
        Logger.d(TAG, "user login error");
    }

}
