/**
 * Bet Activity
 *
 * @desc - handles all of the betting
 * @authors - Tim Mahler, Nick Burrin, Ryan Templin
 * @version - 1.0.0
 */

package com.example.ryantemplin.payoutapp.activities;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ryantemplin.payoutapp.R;
import com.example.ryantemplin.payoutapp.asyncTask.CreateBetsTask;
import com.example.ryantemplin.payoutapp.dialog.PayoutCustomDialog;
import com.example.ryantemplin.payoutapp.pojos.Future;
import com.example.ryantemplin.payoutapp.pojos.Game;
import com.example.ryantemplin.payoutapp.pojos.GameBet;
import com.example.ryantemplin.payoutapp.pojos.LineWinnerBet;
import com.example.ryantemplin.payoutapp.pojos.OverUnderBet;
import com.example.ryantemplin.payoutapp.pojos.SuperStar;
import com.example.ryantemplin.payoutapp.pojos.SuperStarBet;
import com.example.ryantemplin.payoutapp.pojos.Team;
import com.example.ryantemplin.payoutapp.util.DataStore;
import com.example.ryantemplin.payoutapp.util.ImageUtil;
import com.example.ryantemplin.payoutapp.util.MainApp;
import com.example.ryantemplin.payoutapp.util.MoneyUtil;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.HashMap;


public class BetActivity extends Activity
        implements CreateBetsTask.OnCreateBetsListener{

    private static final String TAG = BetActivity.class.getSimpleName();    // Used for debugging

    // Static strings
    private static String superStarMultiplierText = "Will %s hit a HR? (%.2fx HR)";

    // Class variables
    private ImageView mHomeTeamLogoView;
    private ImageView mAwayTeamLogoView;
    private TextView mHomeTeamName;
    private TextView mAwayTeamName;
    private TextView mHomeTeamRecord;
    private TextView mAwayTeamRecord;
    private TextView mRadioHomeTeam;
    private TextView mRadioAwayTeam;
    private RadioButton mRadioButtonAwayTeam;
    private RadioButton mRadioButtonHomeTeam;
    private RadioButton mRadioButtonOver;
    private RadioButton mRadioButtonUnder;
    private RadioButton mRadioButtonSSAwayYes;
    private RadioButton mRadioButtonSSAwayNo;
    private RadioButton mRadioButtonSSHomeYes;
    private RadioButton mRadioButtonSSHomeNo;
    private TextView mSSAwayTeam;
    private TextView mSSHomeTeam;
    private TextView mOverUnderValue;
    private TextView mLineBetValue;
    private TextView mMyBalance;
    private TextView mToastText;
    private EditText mWinnerLineBet;
    private EditText mOverUnderBet;
    private EditText mAwaySuperStarBet;
    private EditText mHomeSuperStarBet;
    private String homeOrAway = "";
    private String overUnder = "";
    private String ssAway = "";
    private String ssHome = "";
    private Team homeTeam;
    private Team awayTeam;
    private String gameId;
    private Game game;
    private int centsBetLine;
    private int centsBetOverUnder;
    private int centsBetAwaySS;
    private int centsBetHomeSS;
    private SuperStar homeSuperStar;
    private SuperStar awaySuperStar;
    private BigDecimal homeSSMultiplier;
    private BigDecimal awaySSMultiplier;
    private Toast toast;

    private int amountPlaced;
    private int totalCredit;

    private CreateBetsTask mCreateBetTask;

    private Button mCancelButton;
    private Button mPlaceBetButton;

    // Progress dialog (loading wheel)
    PayoutCustomDialog mProgress = PayoutCustomDialog.newInstance();

    private String current = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_bet);

        // Get bundle content
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.gameId = extras.getString("game_id");
            // TODO: need to check if this is an existing bet and grab the related data
        } else {
            finish();
        }

        // Get Game & team objects
        game = DataStore.getInstance().getGameHash().get(gameId);
        homeTeam = game.getHomeTeam();
        awayTeam = game.getAwayTeam();
        homeSuperStar = homeTeam.getSuperStar();
        awaySuperStar = awayTeam.getSuperStar();

        /******
        Get XML elements
        ******/
        mMyBalance = ((TextView) findViewById(R.id.tv_my_balance));
        // get the edit texts
        mWinnerLineBet = (EditText) findViewById(R.id.et_winner_line_bet_amount);
        mOverUnderBet = (EditText) findViewById(R.id.et_over_under_bet_amount);
        mAwaySuperStarBet = (EditText) findViewById(R.id.et_away_ss_bet_amount);
        mHomeSuperStarBet = (EditText) findViewById(R.id.et_home_ss_bet_amount);
        mRadioHomeTeam = (TextView) findViewById(R.id.radio_home_team);
        mRadioAwayTeam = (TextView) findViewById(R.id.radio_away_team);

        //grab the home and away team radio buttons from the xml
        mRadioButtonHomeTeam = (RadioButton) findViewById(R.id.radio_home_team);
        mRadioButtonAwayTeam = (RadioButton) findViewById(R.id.radio_away_team);

        //Over and Under buttons radio_over
        mRadioButtonOver = (RadioButton) findViewById(R.id.radio_over);
        mRadioButtonUnder = (RadioButton) findViewById(R.id.radio_under);

        //SSAway buttons
        mRadioButtonSSAwayYes = (RadioButton) findViewById(R.id.radio_yes_away_ss);
        mRadioButtonSSAwayNo = (RadioButton) findViewById(R.id.radio_no_away_ss);

        //Over and Under buttons
        mRadioButtonSSHomeYes = (RadioButton) findViewById(R.id.radio_yes_home_ss);
        mRadioButtonSSHomeNo = (RadioButton) findViewById(R.id.radio_no_home_ss);

        // Get line and over under bet text views
        mOverUnderValue = (TextView) findViewById(R.id.tv_over_under_bet_value);
        mLineBetValue = (TextView) findViewById(R.id.tv_line_bet_value);

        // Find view for superstar  textview
        mSSHomeTeam = (TextView) findViewById(R.id.tv_home_team_ss);
        mSSAwayTeam = (TextView) findViewById(R.id.tv_away_team_ss);

        // Set views
        mHomeTeamLogoView = (ImageView) findViewById(R.id.iv_home_team_logo);
        mHomeTeamName = (TextView) findViewById(R.id.tv_home_team_name);
        mHomeTeamRecord = (TextView) findViewById(R.id.tv_home_team_record);

        // Away team views
        mAwayTeamLogoView = (ImageView) findViewById(R.id.iv_away_team_logo);
        mAwayTeamName = (TextView) findViewById(R.id.tv_away_team_name);
        mAwayTeamRecord = (TextView) findViewById(R.id.tv_away_team_record);

        // Buttons
        mCancelButton = (Button) findViewById(R.id.button_cancel_bet);
        mPlaceBetButton = (Button) findViewById(R.id.button_place_bet);

        /******
        Set XML elements
        ******/
        // Set team view attributes
        mHomeTeamName.setText(homeTeam.getTeamName());
        mAwayTeamName.setText(awayTeam.getTeamName());
        mHomeTeamRecord.setText(homeTeam.getRecord());
        mAwayTeamRecord.setText(awayTeam.getRecord());
        ImageUtil.displayHighResImage(mHomeTeamLogoView, homeTeam.getLogoUrl());
        ImageUtil.displayHighResImage(mAwayTeamLogoView, awayTeam.getLogoUrl());

        // Init balance & my balance view
        mMyBalance.setText(
                MoneyUtil.intToDollar(DataStore.getInstance().getUser().getCredit()));  // Init my balance
        centsBetLine = 0;
        centsBetOverUnder = 0;
        centsBetAwaySS = 0;
        centsBetHomeSS = 0;

        //initialize the custom toast for Bet too much!
        initBetTooMuchToast();

        //Set the text for home team radio button
        mRadioHomeTeam.setText(homeTeam.getTeamName());

        //Set the text for away team radio button
        mRadioAwayTeam.setText(awayTeam.getTeamName());

        //Get the favored team
        String favoredTeamId = game.getGameLineOdds().getTeamId();
        String favoredAbbr = "";
        if (favoredTeamId.equals(game.getAwayTeam().getTeamId())){
            favoredAbbr = game.getAwayTeam().getAbbr();
        }else{
            favoredAbbr = game.getHomeTeam().getAbbr();
        }

        //Set the text for Line and over under bet view
        mLineBetValue.setText("LINE: " +favoredAbbr+ "  - " +game.getGameLineOdds().getLine()+ "0");
        mOverUnderValue.setText("OVER/UNDER: " + game.getGameOverUnderOdds().getOverUnderValue() + " runs");

        //Set the text for superstar name textview
        homeSSMultiplier = new BigDecimal(homeSuperStar.getMySuperStarOdds().getOddsMultiplierYesHR());
        homeSSMultiplier = homeSSMultiplier.setScale(2, BigDecimal.ROUND_HALF_UP);
        awaySSMultiplier = new BigDecimal(awaySuperStar.getMySuperStarOdds().getOddsMultiplierYesHR());
        awaySSMultiplier = awaySSMultiplier.setScale(2, BigDecimal.ROUND_HALF_UP);
        mSSAwayTeam.setText(String.format(superStarMultiplierText, awaySuperStar.getName(), awaySSMultiplier));
        mSSHomeTeam.setText(String.format(superStarMultiplierText, homeSuperStar.getName(), homeSSMultiplier));

        //If you have previously bet on this game, get the total amount you previously bet
        if(DataStore.getInstance().getUser().getFutureBetsHash().get(gameId) != null) {
            amountPlaced = DataStore.getInstance().getUser().getFutureBetsHash().get(gameId).getTotalAmountSpent();
            totalCredit = DataStore.getInstance().getUser().getCredit() + amountPlaced;
        }else{//initialize new bet
            amountPlaced = 0;
            totalCredit = DataStore.getInstance().getUser().getCredit();
        }

        /******
        Set XML listeners for Edit Text changes
        ******/
        mWinnerLineBet.addTextChangedListener(new TextWatcher() {
            DecimalFormat dec = new DecimalFormat("0.00");

            @Override
            public void afterTextChanged(Editable arg0) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                // if there is a value in the edit text (the user has typed in an amount to bet)
                if (!s.toString().equals(current)) {
                    mWinnerLineBet.removeTextChangedListener(this);
                    //these next two lines remove the $ from the string
                    String replaceable = String.format("[%s,.]", NumberFormat.getCurrencyInstance().getCurrency().getSymbol());
                    String cleanString = s.toString().replaceAll(replaceable, "");//clean string is the non dollar version of the value


                    centsBetLine = Integer.parseInt(cleanString);
                    //updates your balance in the red bar at top of the activity
                    BigDecimal parsed = ChangeMyBalanceText(cleanString);
                    //turn dollars into cents
                    centsBetLine = (int)(parsed.doubleValue() * 100);
                    //show winning and bet amount on the page
                    ChangeBetAmountText();
                    ChangeWinningsText();

                    String formatted = NumberFormat.getCurrencyInstance().format(parsed).substring(1);
                    current = formatted;

                    mWinnerLineBet.setText(formatted);
                    mWinnerLineBet.setSelection(formatted.length());

                    mWinnerLineBet.addTextChangedListener(this);
                }
            }
        });
        mOverUnderBet.addTextChangedListener(new TextWatcher() {
            DecimalFormat dec = new DecimalFormat("0.00");

            @Override
            public void afterTextChanged(Editable arg0) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                // if there is a value in the edit text (the user has typed in an amount to bet)
                if (!s.toString().equals(current)) {
                    mOverUnderBet.removeTextChangedListener(this);
                    //these next two lines remove the $ from the string
                    String replaceable = String.format("[%s,.]", NumberFormat.getCurrencyInstance().getCurrency().getSymbol());
                    String cleanString = s.toString().replaceAll(replaceable, "");


                    centsBetOverUnder = Integer.parseInt(cleanString);
                    //updates your balance in the red bar at top of the activity
                    BigDecimal parsed = ChangeMyBalanceText(cleanString);
                    //turn dollars into cents
                    centsBetOverUnder = (int)(parsed.doubleValue() * 100);

                    //show winning and bet amount on the page
                    ChangeBetAmountText();
                    ChangeWinningsText();

                    String formatted = NumberFormat.getCurrencyInstance().format(parsed).substring(1);

                    current = formatted;
                    mOverUnderBet.setText(formatted);
                    mOverUnderBet.setSelection(formatted.length());

                    mOverUnderBet.addTextChangedListener(this);
                }
            }
        });
        mAwaySuperStarBet.addTextChangedListener(new TextWatcher() {
            DecimalFormat dec = new DecimalFormat("0.00");

            @Override
            public void afterTextChanged(Editable arg0) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                // if there is a value in the edit text (the user has typed in an amount to bet)
                if (!s.toString().equals(current)) {
                    mAwaySuperStarBet.removeTextChangedListener(this);

                    //these next two lines remove the $ from the string
                    String replaceable = String.format("[%s,.]", NumberFormat.getCurrencyInstance().getCurrency().getSymbol());
                    String cleanString = s.toString().replaceAll(replaceable, "");


                    centsBetAwaySS = Integer.parseInt(cleanString);

                    //updates your balance in the red bar at top of the activity
                    BigDecimal parsed = ChangeMyBalanceText(cleanString);
                    centsBetAwaySS = (int)(parsed.doubleValue() * 100);

                    //show winning and bet amount on the page
                    ChangeBetAmountText();
                    ChangeWinningsText();

                    //format the string
                    String formatted = NumberFormat.getCurrencyInstance().format(parsed).substring(1);

                    current = formatted;

                    //set the text in the appropriate fashion on the page where the user is betting money
                    mAwaySuperStarBet.setText(formatted);
                    mAwaySuperStarBet.setSelection(formatted.length());

                    mAwaySuperStarBet.addTextChangedListener(this);
                }
            }
        });
        mHomeSuperStarBet.addTextChangedListener(new TextWatcher() {
            DecimalFormat dec = new DecimalFormat("0.00");

            @Override
            public void afterTextChanged(Editable arg0) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                // if there is a value in the edit text (the user has typed in an amount to bet)
                if (!s.toString().equals(current)) {
                    mHomeSuperStarBet.removeTextChangedListener(this);

                    //these next two lines remove the $ from the string
                    String replaceable = String.format("[%s,.]", NumberFormat.getCurrencyInstance().getCurrency().getSymbol());
                    String cleanString = s.toString().replaceAll(replaceable, "");

                    centsBetHomeSS = Integer.parseInt(cleanString);

                    //updates your balance in the red bar at top of the activity
                    BigDecimal parsed = ChangeMyBalanceText(cleanString);
                    centsBetHomeSS = (int)(parsed.doubleValue() * 100);

                    //show winning and bet amount on the page
                    ChangeBetAmountText();
                    ChangeWinningsText();

                    //format the string
                    String formatted = NumberFormat.getCurrencyInstance().format(parsed).substring(1);

                    current = formatted;

                    //set the text in the appropriate fashion on the page where the user is betting money
                    mHomeSuperStarBet.setText(formatted);
                    mHomeSuperStarBet.setSelection(formatted.length());

                    mHomeSuperStarBet.addTextChangedListener(this);
                }
            }
        });

        //ok now you need to when you click have all the values already in the right spot
        //update how much betting and winnings etc
        HashMap<String, GameBet> futureBetHash;
        HashMap<String, GameBet> activeBetHash;

        //get current time and time of game
        Calendar now = Calendar.getInstance();
        Calendar gameDate = DataStore.getInstance().getGameHash().get(gameId).getDate();


        String homeTeamId = DataStore.getInstance().getGameHash().get(gameId).getHomeTeam().getTeamId();
        String awayTeamId = DataStore.getInstance().getGameHash().get(gameId).getAwayTeam().getTeamId();

        // initialize all variables to non meaningful values
        String winnerId = "";
        int overUnder = -1;
        int awayHR = -1;
        int homeHR = -1;
        // compareTO returns 0 if equal <0 if time of calendar is less than argument, greater than 0 if calendar later than argument
        if (now.compareTo(gameDate) < 0){// the game is in the future
            //the user has already made a bet
            if (DataStore.getInstance().getUser().getFutureBetsHash().get(gameId) != null){
                //AUTO POPULATE THE BET AMOUNTS

                //the next few if statements update the bet amounts if the ser has bet on that particular type of bet
                if(DataStore.getInstance().getUser().getFutureBetsHash().get(gameId).getLwb() != null) {
                    centsBetLine = DataStore.getInstance().getUser().getFutureBetsHash().get(gameId).getLwb().getAmount();
                    winnerId = DataStore.getInstance().getUser().getFutureBetsHash().get(gameId).getLwb().getChoiceTeamId();
                }
                if(DataStore.getInstance().getUser().getFutureBetsHash().get(gameId).getOub() != null) {
                    centsBetOverUnder = DataStore.getInstance().getUser().getFutureBetsHash().get(gameId).getOub().getAmount();
                    overUnder = DataStore.getInstance().getUser().getFutureBetsHash().get(gameId).getOub().getOverOrUnder();

                }
                if(DataStore.getInstance().getUser().getFutureBetsHash().get(gameId).getAwaySuperStarBet() != null) {
                    centsBetAwaySS = DataStore.getInstance().getUser().getFutureBetsHash().get(gameId).getAwaySuperStarBet().getAmount();
                    awayHR = DataStore.getInstance().getUser().getFutureBetsHash().get(gameId).getAwaySuperStarBet().getHR();
                }
                if(DataStore.getInstance().getUser().getFutureBetsHash().get(gameId).getHomeSuperStarBet() != null) {
                    centsBetHomeSS = DataStore.getInstance().getUser().getFutureBetsHash().get(gameId).getHomeSuperStarBet().getAmount();
                    homeHR = DataStore.getInstance().getUser().getFutureBetsHash().get(gameId).getHomeSuperStarBet().getHR();
                }

            }
        }else{ //the bet is not in the future
            //the game is currently in the future hash but needs to be moved to the active hash
            if (DataStore.getInstance().getUser().getFutureBetsHash().get(gameId) != null) {
                //put the bet in the Active Hash and remove from future bet hash
                futureBetHash = DataStore.getInstance().getUser().getFutureBetsHash();
                activeBetHash =  DataStore.getInstance().getUser().getActiveBetsHash();
                GameBet game = DataStore.getInstance().getUser().getFutureBetsHash().get(gameId);
                activeBetHash.put(gameId, game);
                DataStore.getInstance().getUser().setActiveBetsHash(activeBetHash);
                futureBetHash.remove(gameId);
                DataStore.getInstance().getUser().setFutureBetsHash(futureBetHash);

            }
            //the game is in the active hash (not in completed hash)
            if (DataStore.getInstance().getUser().getActiveBetsHash().get(gameId) != null) {
                //AUTO POPULATE THE BET AMOUNTS

                //the next few if statements update the bet amounts if the ser has bet on that particular type of bet
                if(DataStore.getInstance().getUser().getActiveBetsHash().get(gameId).getLwb() != null) {
                    centsBetLine = DataStore.getInstance().getUser().getActiveBetsHash().get(gameId).getLwb().getAmount();
                    winnerId = DataStore.getInstance().getUser().getActiveBetsHash().get(gameId).getLwb().getChoiceTeamId();
                }
                if(DataStore.getInstance().getUser().getActiveBetsHash().get(gameId).getOub() != null) {
                    centsBetOverUnder = DataStore.getInstance().getUser().getActiveBetsHash().get(gameId).getOub().getAmount();
                    overUnder = DataStore.getInstance().getUser().getActiveBetsHash().get(gameId).getOub().getOverOrUnder();

                }
                if(DataStore.getInstance().getUser().getActiveBetsHash().get(gameId).getAwaySuperStarBet() != null) {
                    centsBetAwaySS = DataStore.getInstance().getUser().getActiveBetsHash().get(gameId).getAwaySuperStarBet().getAmount();
                    awayHR = DataStore.getInstance().getUser().getActiveBetsHash().get(gameId).getAwaySuperStarBet().getHR();
                }
                if(DataStore.getInstance().getUser().getActiveBetsHash().get(gameId).getHomeSuperStarBet() != null) {
                    centsBetHomeSS = DataStore.getInstance().getUser().getActiveBetsHash().get(gameId).getHomeSuperStarBet().getAmount();
                    homeHR = DataStore.getInstance().getUser().getActiveBetsHash().get(gameId).getHomeSuperStarBet().getHR();
                }
            }else { //the game is in the completed
                //AUTO POPULATE THE BET AMOUNTS

                //the next few if statements update the bet amounts if the ser has bet on that particular type of bet
                if(DataStore.getInstance().getUser().getCompletedBetsHash().get(gameId).getLwb() != null) {
                    centsBetLine = DataStore.getInstance().getUser().getCompletedBetsHash().get(gameId).getLwb().getAmount();
                    winnerId = DataStore.getInstance().getUser().getCompletedBetsHash().get(gameId).getLwb().getChoiceTeamId();
                }
                if(DataStore.getInstance().getUser().getCompletedBetsHash().get(gameId).getOub() != null) {
                    centsBetOverUnder = DataStore.getInstance().getUser().getCompletedBetsHash().get(gameId).getOub().getAmount();
                    overUnder = DataStore.getInstance().getUser().getCompletedBetsHash().get(gameId).getOub().getOverOrUnder();

                }
                if(DataStore.getInstance().getUser().getCompletedBetsHash().get(gameId).getAwaySuperStarBet() != null) {
                    centsBetAwaySS = DataStore.getInstance().getUser().getCompletedBetsHash().get(gameId).getAwaySuperStarBet().getAmount();
                    awayHR = DataStore.getInstance().getUser().getCompletedBetsHash().get(gameId).getAwaySuperStarBet().getHR();
                }
                if(DataStore.getInstance().getUser().getCompletedBetsHash().get(gameId).getAwaySuperStarBet() != null) {
                    centsBetHomeSS = DataStore.getInstance().getUser().getCompletedBetsHash().get(gameId).getHomeSuperStarBet().getAmount();
                    homeHR = DataStore.getInstance().getUser().getCompletedBetsHash().get(gameId).getHomeSuperStarBet().getHR();
                }
            }

            // can't change your bets if active or completed
            mWinnerLineBet.setFocusable(false);
            mOverUnderBet.setFocusable(false);
            mAwaySuperStarBet.setFocusable(false);
            mHomeSuperStarBet.setFocusable(false);
            mRadioButtonHomeTeam.setEnabled(false);
            mRadioButtonAwayTeam.setEnabled(false);
            mRadioButtonUnder.setEnabled(false);
            mRadioButtonOver.setEnabled(false);
            mRadioButtonSSAwayYes.setEnabled(false);
            mRadioButtonSSAwayNo.setEnabled(false);
            mRadioButtonSSHomeYes.setEnabled(false);
            mRadioButtonSSHomeNo.setEnabled(false);



        }

        //set text
        mWinnerLineBet.setText(Integer.toString(centsBetLine));
        mOverUnderBet.setText(Integer.toString(centsBetOverUnder));
        mAwaySuperStarBet.setText(Integer.toString(centsBetAwaySS));
        mHomeSuperStarBet.setText(Integer.toString(centsBetHomeSS));

        //these next few ifs highlight the radio buttons appropriately
        if (homeTeamId.equals(winnerId)) {
            mRadioButtonHomeTeam.toggle();
        }else if (awayTeamId.equals(winnerId)){
            mRadioButtonAwayTeam.toggle();
        }
        //populating overUnder radio

        if (overUnder == 0){
            mRadioButtonUnder.toggle();
        } else if (overUnder == 1){
            mRadioButtonOver.toggle();
        }
        //populating awayHR radio

        if (awayHR == 1){
            mRadioButtonSSAwayYes.toggle();
        } else if (awayHR == 0){
            mRadioButtonSSAwayNo.toggle();
        }
        //populating homeHR radio

        if (homeHR == 1){
            mRadioButtonSSHomeYes.toggle();
        } else if (homeHR == 0){
            mRadioButtonSSHomeNo.toggle();
        }


        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mPlaceBetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptPlaceBets();
            }
        });
    }

    /*
     * Initialize the custom toast view for bet too much
     */
    private void initBetTooMuchToast(){
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.view_toast_layout,
                (ViewGroup) findViewById(R.id.toast_layout_root));

        mToastText = (TextView) layout.findViewById(R.id.text);

        // Set custom toast
        mToastText.setText("You Bet Too Much!!");   // Set toast message

        toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
    }

    /**
     * changes the balance text in the bet activity
     * @param cleanString
     * @return
     */
    public BigDecimal ChangeMyBalanceText(String cleanString){
        BigDecimal parsed = new BigDecimal(cleanString).setScale(2, BigDecimal.ROUND_FLOOR).divide(new BigDecimal(100), BigDecimal.ROUND_FLOOR);
        //make cleanString an int
        int myBalance = DataStore.getInstance().getUser().getCredit();

        //if you bet less than you have, then show the decremented version of your balance
        Calendar now = Calendar.getInstance();
        Calendar gameDate = DataStore.getInstance().getGameHash().get(gameId).getDate();
        int value = ((centsBetLine + centsBetOverUnder + centsBetAwaySS + centsBetHomeSS)- amountPlaced);

        //the total value of what has been bet is less that your total balance remaining
        if (value <= myBalance) {
            // compareTO returns 0 if equal <0 if time of calendar is less than argument, greater than 0 if calendar later than argument
            if (now.compareTo(gameDate) < 0) {//if the bet is editable
                myBalance -= ((centsBetLine + centsBetOverUnder + centsBetAwaySS + centsBetHomeSS) - amountPlaced);
            }
            mMyBalance.setText(
                    MoneyUtil.intToDollar(myBalance));
        }else{// you have bet more than you have

            //BetTooMuch();
            if (now.compareTo(gameDate) < 0) {
                parsed = new BigDecimal(cleanString).setScale(2, BigDecimal.ROUND_FLOOR).divide(new BigDecimal(1000), BigDecimal.ROUND_FLOOR);
                toast.show();
            }

        }
        return parsed;
    }

    /**
     * changes the amount bet text
     * @param
     */
    public void ChangeBetAmountText(){
        int myBalance = DataStore.getInstance().getUser().getCredit();

        //if you bet less than you have, then show the decremented version of your balance
        if ( ((centsBetLine + centsBetOverUnder + centsBetAwaySS + centsBetHomeSS)-amountPlaced) <= myBalance) {
            int betAmount = (centsBetLine + centsBetOverUnder + centsBetAwaySS + centsBetHomeSS);
            ((TextView) findViewById(R.id.tv_betting)).setText(
                    MoneyUtil.intToDollar(betAmount));
        }
    }

    /**
     * changes the winnings bet text
     *
     */
    public void ChangeWinningsText(){

        int myBalance = DataStore.getInstance().getUser().getCredit();

        //if you bet less than you have, then show the decremented version of your balance
        if ( ((centsBetLine + centsBetOverUnder + centsBetAwaySS + centsBetHomeSS)-amountPlaced) <= myBalance) {
            double doubleWinningsAmount = (double) ((centsBetLine*2) + (centsBetOverUnder*2) + centsBetAwaySS + (centsBetAwaySS * awaySSMultiplier.doubleValue()) + centsBetHomeSS + (centsBetHomeSS * homeSSMultiplier.doubleValue()));
            int winningsAmount = (int) doubleWinningsAmount;
            ((TextView) findViewById(R.id.tv_winnings)).setText(
                    MoneyUtil.intToDollar(winningsAmount));
        }
    }


    /**
     * This method prompts the user to make a bet before
     */
    public void InvalidBet(){
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.view_toast_layout,
                (ViewGroup) findViewById(R.id.toast_layout_root));

        TextView text = (TextView) layout.findViewById(R.id.text);
        text.setText("Make a Valid Bet!!");

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    public void attemptPlaceBets(){

        //get the amounts to string  form
        String winnerLineBet = mWinnerLineBet.getText().toString();
        String overUnderBet = mOverUnderBet.getText().toString();
        String awaySuperStarBet = mAwaySuperStarBet.getText().toString();
        String homeSuperStarBet = mHomeSuperStarBet.getText().toString();
        boolean winnerBetValid = true;
        boolean overUnderBetValid = true;
        boolean awaySuperStarBetValid = true;
        boolean homeSuperStarBetValid = true;


        //get the amounts to double form (must check if null, else throws NumberFormatException)
        int winnerLineAmount;

        // the next few if statements check if the amount is greater than $0
        if(!winnerLineBet.equals("0.00")){
            //make bet amount as cents
            winnerLineAmount = (int) ((Double.parseDouble(winnerLineBet))*100);
        } else{
            //show amount bet is 0 and don't let any of the radios be filled in
            winnerLineAmount = 0;
            centsBetLine = 0;
            winnerBetValid = false;
        }

        int overUnderAmount;
        if(!overUnderBet.equals("0.00")){
            //make bet amount as cents
            overUnderAmount = (int) ((Double.parseDouble(overUnderBet))*100);
        } else{
            //show amount bet is 0 and don't let any of the radios be filled in
            overUnderAmount = 0;
            centsBetOverUnder = 0;
            overUnderBetValid = false;
        }

        int awaySuperStarAmount;
        if(!awaySuperStarBet.equals("0.00")){
            //make bet amount as cents
            awaySuperStarAmount = (int) ((Double.parseDouble(awaySuperStarBet))*100);
        } else{
            //show amount bet is 0 and don't let any of the radios be filled in
            awaySuperStarAmount = 0;
            centsBetAwaySS = 0;
            awaySuperStarBetValid = false;
        }

        int homeSuperStarAmount;
        if(!homeSuperStarBet.equals("0.00")){
            //make bet amount as cents
            homeSuperStarAmount = (int) ((Double.parseDouble(homeSuperStarBet))*100);
        } else{
            //show amount bet is 0 and don't let any of the radios be filled in
            homeSuperStarAmount = 0;
            centsBetHomeSS = 0;
            homeSuperStarBetValid = false;
        }

        //get the team from the RadioButton
        String betTeamId = null;
        //if the user has input who they think will win the game
        if (homeOrAway.equals("home")){
            betTeamId = homeTeam.getTeamId();
        }else if(homeOrAway.equals("away")){
            betTeamId = awayTeam.getTeamId();
        }else if (DataStore.getInstance().getUser().getFutureBetsHash().get(gameId) != null) {//if there is a previous bet
            if (DataStore.getInstance().getUser().getFutureBetsHash().get(gameId).getLwb() != null) {//if there is a previous LineBet
                betTeamId = DataStore.getInstance().getUser().getFutureBetsHash().get(gameId).
                        getLwb().getChoiceTeamId();
            }else{
                // there is not a previous line bet, thus invalid
                betTeamId = null;
                winnerLineAmount = 0;
                centsBetLine = 0;
                winnerBetValid = false;
            }
        }else{
            //Invalid choice
            betTeamId = null;
            winnerLineAmount = 0;
            centsBetLine = 0;
            winnerBetValid = false;
        }

        //get the overUnder from the RadioButton
        int choice = -1;
        if (overUnder.equals("over")){
            choice = OverUnderBet.OVER;
        }else if(overUnder.equals("under")){
            choice = OverUnderBet.UNDER;
        }else if (DataStore.getInstance().getUser().getFutureBetsHash().get(gameId) != null) {//if there is a previous bet
            if (DataStore.getInstance().getUser().getFutureBetsHash().get(gameId).getOub() != null) {//if there is a previous OverUnderBet
                choice = DataStore.getInstance().getUser().getFutureBetsHash().get(gameId).
                        getOub().getOverOrUnder();
            }else{
                // Else overUnder is null, thus invalid
                choice = -1;
                overUnderAmount = 0;
                centsBetOverUnder = 0;
                overUnderBetValid = false;
            }
        }else{//not a previous bet
            choice = -1;
            overUnderAmount = 0;
            centsBetOverUnder = 0;
            overUnderBetValid = false;
        }

        //get the ssAway from the RadioButton
        int ssAway = -1;
        if (this.ssAway.equals("yes")){
            ssAway = 1;
        }else if(this.ssAway.equals("no")){
            ssAway = 0;
        }else if (DataStore.getInstance().getUser().getFutureBetsHash().get(gameId) != null) {//if there is a previous bet
            if (DataStore.getInstance().getUser().getFutureBetsHash().get(gameId).getAwaySuperStarBet() != null) {//if there is a previous superstarBet
                ssAway = DataStore.getInstance().getUser().getFutureBetsHash().get(gameId).
                        getAwaySuperStarBet().getHR();
            }else{
                // Else ssAway is null, thus invalid
                ssAway = -1;
                awaySuperStarAmount = 0;
                centsBetAwaySS = 0;
                awaySuperStarBetValid = false;
            }
        }else{//didn't select over or under radio button
            ssAway = -1;
            awaySuperStarAmount = 0;
            centsBetAwaySS = 0;
            awaySuperStarBetValid = false;

        }

        //get the ssHome from the RadioButton
        int ssHome = -1;
        if (this.ssHome.equals("yes")){
            ssHome = 1;
        }else if(this.ssHome.equals("no")){
            ssHome = 0;
        }else if (DataStore.getInstance().getUser().getFutureBetsHash().get(gameId) != null) {//if there is a previous bet
            if (DataStore.getInstance().getUser().getFutureBetsHash().get(gameId).getHomeSuperStarBet() != null) {//if there is a previous superstarBet
                ssHome = DataStore.getInstance().getUser().getFutureBetsHash().get(gameId).
                        getHomeSuperStarBet().getHR();
            }else{
                // Else ssHome is null, thus invalid
                ssHome = -1;
                homeSuperStarAmount = 0;
                centsBetHomeSS = 0;
                homeSuperStarBetValid = false;
            }
        }else {// not a previous bet
            ssHome = -1;
            homeSuperStarAmount = 0;
            centsBetHomeSS = 0;
            homeSuperStarBetValid = false;
        }

        int totalSpent = centsBetLine+centsBetOverUnder+centsBetAwaySS+centsBetHomeSS;
        // Add bet only if inputs have been filled in and total less than or equal to total credit
        if ( (winnerBetValid || overUnderBetValid || awaySuperStarBetValid || homeSuperStarBetValid) && (totalSpent <= totalCredit) ){
            //creating bet objects (assign null data_id b/c haven't send to back end yet)
            mProgress.show(getFragmentManager(), "dialog");//wait dialog loader

            //gather total amount spent
            Integer totalAmountSpent = winnerLineAmount + overUnderAmount + awaySuperStarAmount + homeSuperStarAmount;

            //create bet objects for each type of bet
            LineWinnerBet myWinnerBet = winnerBetValid? new LineWinnerBet(this.gameId, null, winnerLineAmount, betTeamId) : null;

            OverUnderBet myOverUnderBet = overUnderBetValid? new OverUnderBet(this.gameId, null, overUnderAmount, choice) : null;

            SuperStarBet myAwaySuperStarBet = awaySuperStarBetValid? new SuperStarBet(this.gameId, null, awaySuperStarAmount,
                    DataStore.getInstance().getGameHash().get(gameId).getAwayTeam().getSuperStar().getMySuperStarOdds().getOddsMultiplierYesHR(),
                    game.getAwayTeam().getSuperStar().getPlayerId(), ssAway) : null;

            SuperStarBet myHomeSuperStarBet = homeSuperStarBetValid? new SuperStarBet(this.gameId, null, homeSuperStarAmount,
                    DataStore.getInstance().getGameHash().get(gameId).getHomeTeam().getSuperStar().getMySuperStarOdds().getOddsMultiplierYesHR(),
                    game.getHomeTeam().getSuperStar().getPlayerId(), ssHome) : null;

            //put all types of bets in a gameBet object
            GameBet myGameBet = new GameBet(this.gameId, new Future(), myWinnerBet, myOverUnderBet,
                    myAwaySuperStarBet, myHomeSuperStarBet, totalAmountSpent);

            Log.d(TAG, "Game Bet winnerBetValid:" + winnerBetValid);
            Log.d(TAG, "Game Bet overUnderBetValid:" + overUnderBetValid);
            Log.d(TAG, "Game Bet awaySuperStarBetValid:" + awaySuperStarBetValid);
            Log.d(TAG, "Game Bet homeSuperStarBetValid:" + homeSuperStarBetValid);
            Log.d(TAG, "Game Bet:" + myGameBet);

            //put the game bet in a hash
            DataStore.getInstance().getUser().getFutureBetsHash().put(gameId, myGameBet); // Add bet to hash

            mCreateBetTask = new CreateBetsTask(myGameBet, this, (MainApp) this.getApplication());//, (MainApp)this.getActivity().getApplication());
            mCreateBetTask.execute();
        }else{//bet is not valid
            InvalidBet();
        }
    }

    /**
     * when a radio button is clicked
     * @param view
     */
    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        if (checked){
            ((RadioButton) view).toggle();
        }
        // Check which radio button was clicked by the user
        //will signal where the radio button is
        switch(view.getId()) {
            case R.id.radio_away_team:
                if (checked)
                    homeOrAway = "away";
                break;
            case R.id.radio_home_team:
                if (checked)
                    homeOrAway = "home";
                break;
            case R.id.radio_over:
                if (checked)
                    overUnder = "over";
                break;
            case R.id.radio_under:
                if (checked)
                    overUnder = "under";
                break;
            case R.id.radio_yes_away_ss:
                if (checked)
                    ssAway = "yes";
                    //get the multiplier
                    awaySSMultiplier = new BigDecimal((awaySuperStar.getMySuperStarOdds().getOddsMultiplierYesHR()));
                    awaySSMultiplier = awaySSMultiplier.setScale(2, BigDecimal.ROUND_HALF_UP);
                    mSSAwayTeam.setText(String.format(superStarMultiplierText, awaySuperStar.getName(), awaySSMultiplier));
                    ChangeWinningsText();
                break;
            case R.id.radio_no_away_ss:
                if (checked)
                    ssAway = "no";
                    //get the multiplier
                    awaySSMultiplier = new BigDecimal(1/(awaySuperStar.getMySuperStarOdds().getOddsMultiplierYesHR()));
                    awaySSMultiplier = awaySSMultiplier.setScale(2, BigDecimal.ROUND_HALF_UP);
                    mSSAwayTeam.setText(String.format(superStarMultiplierText, awaySuperStar.getName(), awaySSMultiplier));
                    ChangeWinningsText();
                break;
            case R.id.radio_yes_home_ss:
                if (checked)
                    ssHome = "yes";
                    //get the multiplier
                    homeSSMultiplier = new BigDecimal((homeSuperStar.getMySuperStarOdds().getOddsMultiplierYesHR()));
                    homeSSMultiplier = homeSSMultiplier.setScale(2, BigDecimal.ROUND_HALF_UP);
                    mSSHomeTeam.setText(String.format(superStarMultiplierText, homeSuperStar.getName(), homeSSMultiplier));
                    ChangeWinningsText();
                break;
            case R.id.radio_no_home_ss:
                if (checked)
                    ssHome = "no";
                    //get the multiplier
                    homeSSMultiplier = new BigDecimal(1/(homeSuperStar.getMySuperStarOdds().getOddsMultiplierYesHR()));
                    homeSSMultiplier = homeSSMultiplier.setScale(2, BigDecimal.ROUND_HALF_UP);
                    mSSHomeTeam.setText(String.format(superStarMultiplierText, homeSuperStar.getName(), homeSSMultiplier));
                    ChangeWinningsText();
                break;

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mProgress.closeDialog();
    }

    @Override
    public void OnCreateBetsSuccess() {
        mProgress.closeDialog();
        DataStore.getInstance().getUser().decrementTotalCredit(((centsBetLine + centsBetOverUnder + centsBetAwaySS + centsBetHomeSS) - amountPlaced));
        finish();
    }

    @Override
    public void OnCreateBetsError(String message) {
        mProgress.closeDialog();
    }
}

