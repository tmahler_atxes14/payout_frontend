/**
 * Cash Out Activity
 *
 * @desc - handles if the user decides to cash out their winnings, if you have over $100
 * @authors - Tim Mahler, Nick Burrin, Ryan Templin
 * @version - 1.0.0
 */

package com.example.ryantemplin.payoutapp.activities;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ryantemplin.payoutapp.R;
import com.example.ryantemplin.payoutapp.asyncTask.CashOutTask;
import com.example.ryantemplin.payoutapp.asyncTask.LoginTask;
import com.example.ryantemplin.payoutapp.dialog.PayoutCustomDialog;
import com.example.ryantemplin.payoutapp.util.DataStore;
import com.example.ryantemplin.payoutapp.util.MainApp;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class CashOutActivity extends Activity implements CashOutTask.OnCashOutListener{

    private static final String TAG = CashOutActivity.class.getSimpleName();

    EditText mNameTextView;
    EditText mAddrTextView;
    EditText mZipTextView;
    EditText mCityTextView;
    EditText mStateTextView;
    EditText mAmountTextView;
    TextView myBalanceTextView;

    private TextView mToastText;
    private Toast toast;

    public int amount;
    private int myBalance;

    public CashOutTask mCashOutTask;

    // Progress dialog (loading wheel)
    PayoutCustomDialog mProgress = PayoutCustomDialog.newInstance();


    Button mCancelButton;
    Button mCashOut;

    private static String ERROR_AMOUNT_EXCEED = "You Bet Too Much!!";
    private static String ERROR_AMOUNT_UNDER = "Cannot cash out less than $100!!";
    private static String MY_BALANCE = "MY BALANCE: $%.02f";

    private String current = ""; // Used for the edit text listner

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cash_out);

        // Get XML layout params
        mNameTextView = (EditText) findViewById(R.id.et_name);
        mAddrTextView = (EditText) findViewById(R.id.et_address);
        mZipTextView = (EditText) findViewById(R.id.et_zipcode);
        mCityTextView = (EditText) findViewById(R.id.et_city);
        mStateTextView = (EditText) findViewById(R.id.et_state);
        mAmountTextView = (EditText) findViewById(R.id.et_amount_cash_out);
        mCancelButton = (Button) findViewById(R.id.button_cancel_bet);
        mCashOut = (Button) findViewById(R.id.button_cash_out);
        myBalanceTextView = (TextView) findViewById(R.id.tv_my_balance);

        // Set default cash out amount to $100
        myBalance = DataStore.getInstance().getUser().getCredit();
        myBalanceTextView.setText(String.format(MY_BALANCE, ((myBalance-10000)/100.0)));
        amount = 10000;
        mAmountTextView.setText("$100.00");

        // Init toast
        initCustomToast();

        // Set listeners
        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        mCashOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                cashOutHandler();
            }
        });
        mAmountTextView.addTextChangedListener(new TextWatcher() {
            DecimalFormat dec = new DecimalFormat("0.00");

            @Override
            public void afterTextChanged(Editable arg0) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                // if there is a value in the edit text (the user has typed in an amount to bet)
                if (!s.toString().equals(current)) {
                    mAmountTextView.removeTextChangedListener(this);

                    //these next two lines remove the $ from the string
                    String replaceable = String.format("[%s,.]", NumberFormat.getCurrencyInstance().getCurrency().getSymbol());
                    String cleanString = s.toString().replaceAll(replaceable, "");

                    amount = Integer.parseInt(cleanString);

                    BigDecimal parsed = new BigDecimal(cleanString).setScale(2, BigDecimal.ROUND_FLOOR).divide(new BigDecimal(100), BigDecimal.ROUND_FLOOR);
                    //make cleanString an int
                    int myBalance = DataStore.getInstance().getUser().getCredit();

                    if(amount > myBalance){
                        parsed = new BigDecimal(cleanString).setScale(2, BigDecimal.ROUND_FLOOR).divide(new BigDecimal(1000), BigDecimal.ROUND_FLOOR);
                        //BetTooMuch();
                        mToastText.setText(ERROR_AMOUNT_EXCEED);
                        toast.show();
                    }else{
                        myBalanceTextView.setText(String.format(MY_BALANCE, ((myBalance-amount)/100.0)));
                    }

                    String formatted = NumberFormat.getCurrencyInstance().format(parsed);

                    // Set text
                    current = formatted;
                    mAmountTextView.setText(formatted);
                    mAmountTextView.setSelection(formatted.length());
                    mAmountTextView.addTextChangedListener(this);
                }
            }
        });
    }

/*
 * Initialize the custom toast view
 */
    private void initCustomToast(){
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.view_toast_layout,
                (ViewGroup) findViewById(R.id.toast_layout_root));

        mToastText = (TextView) layout.findViewById(R.id.text);

        // Set custom toast
        mToastText.setText(ERROR_AMOUNT_EXCEED);   // Set toast message

        toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
    }

    /**
     * make sure each text field is entered
     */
    public void cashOutHandler(){
        // Parse all of the attributes off of the XML
        String name = mNameTextView.getText().toString();
        String address = mAddrTextView.getText().toString();
        String areaCode = mZipTextView.getText().toString();
        String city = mCityTextView.getText().toString();
        String state = mStateTextView.getText().toString();
        String amountText = mAmountTextView.getText().toString();


        boolean cancel = false;
        View focusView = null;


        // Check if name entered
        if (TextUtils.isEmpty(name)) {
            mNameTextView.setError(getString(R.string.error_field_required));
            focusView = mNameTextView;
            cancel = true;
        }
        // Check if addr entered
        if (TextUtils.isEmpty(address)) {
            mAddrTextView.setError(getString(R.string.error_field_required));
            focusView = mAddrTextView;
            cancel = true;
        }
        // Check if zip entered
        if (TextUtils.isEmpty(areaCode)) {
            mZipTextView.setError(getString(R.string.error_field_required));
            focusView = mZipTextView;
            cancel = true;
        }
        // Check if city entered
        if (TextUtils.isEmpty(city)) {
            mCityTextView.setError(getString(R.string.error_field_required));
            focusView = mCityTextView;
            cancel = true;
        }
        // Check if state entered
        if (TextUtils.isEmpty(state)) {
            mStateTextView.setError(getString(R.string.error_field_required));
            focusView = mStateTextView;
            cancel = true;
        }

        // Check if amount entered
        if (TextUtils.isEmpty(amountText)) {
            mAmountTextView.setError(getString(R.string.error_field_required));
            focusView = mAmountTextView;
            cancel = true;
        }else if(amount < 10000){
            mAmountTextView.setError(ERROR_AMOUNT_UNDER);
            focusView = mAmountTextView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner and cash out user (post to backend)
            mProgress.show(getFragmentManager(), "dialog");
            mCashOutTask = new CashOutTask(this, (MainApp) this.getApplication(),
                    name, address, areaCode, city, state, amount);
            mCashOutTask.execute();
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cash_out, menu);
        return true;


    }

    @Override
    protected void onPause() {
        super.onPause();
        mProgress.closeDialog();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void OnCashoutSuccess() {
        DataStore.getInstance().getUser().decrementTotalCredit(amount);
        mProgress.closeDialog();
        finish();
    }

    @Override
    public void OnCashoutError(String message) {
// Display error message to user
        Toast err = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG);
        err.show();
        mProgress.closeDialog();
    }
}
