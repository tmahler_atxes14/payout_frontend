/**
 * Login Activity
 *
 * @desc - handles the login of the application
 * @authors - Tim Mahler, Nick Burrin, Ryan Templin
 * @version - 1.0.0
 */

package com.example.ryantemplin.payoutapp.activities;


import android.app.Activity;
import android.content.Intent;

import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ryantemplin.payoutapp.R;
import com.example.ryantemplin.payoutapp.asyncTask.GetUserTask;
import com.example.ryantemplin.payoutapp.asyncTask.LoginTask;
import com.example.ryantemplin.payoutapp.dialog.PayoutCustomDialog;
import com.example.ryantemplin.payoutapp.util.Logger;
import com.example.ryantemplin.payoutapp.util.MainApp;
import com.example.ryantemplin.payoutapp.util.Memory;

import android.support.v4.app.FragmentManager;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends Activity
        implements LoginTask.OnLoginListener,
        GetUserTask.OnGetUserListener{

    private static final String TAG = LoginActivity.class.getSimpleName();

    // Progress dialog (loading wheel)
    PayoutCustomDialog mProgress = PayoutCustomDialog.newInstance();

    private LoginTask mAuthTask;

    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mLoginFormView;
    private FragmentManager fm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        
        // Do the first time check to see if user is logged in
        checkAuth();

        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.et_email);

        mPasswordView = (EditText) findViewById(R.id.et_password);
        mPasswordView.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if ((keyEvent != null && (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (id == EditorInfo.IME_ACTION_DONE)) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        // if they hit the login button, go to login method
        Button mEmailSignInButton = (Button) findViewById(R.id.button_login);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        // if they hit the sign up button, go to register method
        Button mEmailRegisterButton = (Button) findViewById(R.id.button_register);
        mEmailRegisterButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptRegister();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
    }

    /**
     * Checks if auth token stored, then load user profile
     */
    private void checkAuth() {
        // Set params
        int userId = Memory.getInstance(this).getUserId();
        Logger.d(TAG, "checkAuth: retrieved userId from memory --> "+userId);

        if(userId == Integer.MIN_VALUE){
            // Not logged in
            return;
        }else{
            // Logged in, redirect to games view
            GetUserTask getUserTask = new GetUserTask(userId, this, (MainApp)this.getApplication());
            getUserTask.execute();
        }
    }

    /**
     * attempts to register the user by going to the next activity
     */
    public void attemptRegister(){
        Intent i = new Intent(LoginActivity.this, RegisterUserActivity.class);
        startActivity(i);
        overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
    }
    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    public void attemptLogin() {

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;


        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            mProgress.show(getFragmentManager(), "dialog");
            mAuthTask = new LoginTask(email, password, this);
            mAuthTask.execute();
        }
    }

    /**
     * check validity of email user in Login
     * @param email
     * @return
     */
    private boolean isEmailValid(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    /**
     * check to confirm password
     * @param password
     * @return
     */
    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }


    @Override
    protected void onPause() {
        super.onPause();
        mProgress.closeDialog();
    }

    @Override
    public void onLoginSuccess(int id) {
        // If login is successful, then get user profile
        // On Success of registering user, get user profile
        GetUserTask getUserTask = new GetUserTask(id, this, (MainApp)this.getApplication());
        getUserTask.execute();

    }

    @Override
    public void onLoginError(String message) {
        // If login error, print error message
        Toast err = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
        err.show();
        mProgress.closeDialog();
    }

    @Override
    public void onGetUserSuccess() {
        // If successfully got user profile, then go to main activity
        Logger.d(TAG, "Successfully got the user profile");
        mProgress.closeDialog();
        Intent i = new Intent(LoginActivity.this, HomeTabsActivity.class);
        startActivity(i);
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        finish();
    }

    @Override
    public void onGetUserError(String message) {
        // Display error message to user
        Toast err = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
        err.show();
        mProgress.closeDialog();
    }

}



