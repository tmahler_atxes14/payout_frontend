/**
 * My Tab Listener Activity
 *
 * @desc - Listens for a change in tab by clicking a tab or swipe
 * @authors - Tim Mahler, Nick Burrin, Ryan Templin
 * @version - 1.0.0
 */

package com.example.ryantemplin.payoutapp.activities;

import android.app.ActionBar;
import android.app.Fragment;
import android.support.v4.view.ViewPager;


import com.example.ryantemplin.payoutapp.R;

/**
 * Created by ryantemplin on 4/10/15.
 */
public class MyTabListener implements ActionBar.TabListener {
   // Fragment fragment;
    ViewPager mViewPager;

    public MyTabListener(ViewPager viewPager) {
        this.mViewPager = viewPager;
    }

    public void onTabSelected(android.app.ActionBar.Tab tab, android.app.FragmentTransaction ft) {
        mViewPager.setCurrentItem(tab.getPosition());
    }

    public void onTabUnselected(android.app.ActionBar.Tab tab, android.app.FragmentTransaction ft) {
        //nothing done here
    }

    public void onTabReselected(android.app.ActionBar.Tab tab, android.app.FragmentTransaction ft) {
        // nothing done here
    }
}
