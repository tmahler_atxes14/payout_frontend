/**
 * Register User Activity
 *
 * @desc - page that allows a user to register
 * @authors - Tim Mahler, Nick Burrin, Ryan Templin
 * @version - 1.0.0
 */

package com.example.ryantemplin.payoutapp.activities;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ryantemplin.payoutapp.R;


import com.example.ryantemplin.payoutapp.asyncTask.GetUserTask;
import com.example.ryantemplin.payoutapp.asyncTask.RegisterUserTask;
import com.example.ryantemplin.payoutapp.dialog.PayoutCustomDialog;
import com.example.ryantemplin.payoutapp.util.Logger;
import com.example.ryantemplin.payoutapp.util.MainApp;


/**
 * A login screen that offers login via email/password.
 */
public class RegisterUserActivity extends Activity
        implements RegisterUserTask.OnRegisterListener,
        GetUserTask.OnGetUserListener{

    private static final String TAG = RegisterUserActivity.class.getSimpleName();

    // Progress dialog (loading wheel)
    PayoutCustomDialog mProgress = PayoutCustomDialog.newInstance();

    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private RegisterUserTask mAuthTask = null;

    // UI references.
    private EditText firstName;
    private EditText lastName;
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private EditText mConfirmPasswordView;
    private Button mRegisterButton;
    private Button mExistingUserButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        // Set up the login form.
        firstName = (EditText) findViewById(R.id.et_first_name);
        lastName = (EditText) findViewById(R.id.et_last_name);
        mEmailView = (AutoCompleteTextView) findViewById(R.id.et_email);

        mPasswordView = (EditText) findViewById(R.id.et_password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        mConfirmPasswordView = (EditText) findViewById(R.id.et_confirm_password);

        mRegisterButton = (Button) findViewById(R.id.button_register);
        mRegisterButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mExistingUserButton = (Button) findViewById(R.id.button_existing_user);
        mExistingUserButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    public void attemptLogin() {

        // Reset errors.
        firstName.setError(null);
        lastName.setError(null);
        mEmailView.setError(null);
        mPasswordView.setError(null);
        mConfirmPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String first = firstName.getText().toString();
        String last = lastName.getText().toString();
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();
        String confirmPassword = mConfirmPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if(first.isEmpty()){
            // Check for first name
            firstName.setError(getString(R.string.error_field_required));
            focusView = firstName;
            cancel = true;
        }
        if(last.isEmpty()){
            // Check for last name
            lastName.setError(getString(R.string.error_field_required));
            focusView = lastName;
            cancel = true;
        }
        if (TextUtils.isEmpty(password)) {
            // Check if the user entered password
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        }
        if (TextUtils.isEmpty(confirmPassword)) {
            // Check if the user entered confirm password
            mConfirmPasswordView.setError(getString(R.string.error_field_required));
            focusView = mConfirmPasswordView;
            cancel = true;
        }
        if (TextUtils.isEmpty(email)) {
            // Check if user entered email
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        }
        if (!isEmailValid(email)) {
            // Check for a valid email address.
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }
        if(!doPasswordsMatch(password, confirmPassword)){
            // Check if confirm password matches the original, if the user entered one.
            mConfirmPasswordView.setError(getString(R.string.error_password_no_match));
            focusView = mConfirmPasswordView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            mProgress.show(getFragmentManager(), "dialog");
            mAuthTask = new RegisterUserTask(first, last, email, password, this);
            mAuthTask.execute();
        }
    }

    /**
     * checks to see if the password the user types in matches the password saved in the backend
     * @param pass
     * @param confirm
     * @return
     */
    private boolean doPasswordsMatch(String pass, String confirm) {
        if(pass.isEmpty() || confirm.isEmpty()) {
            return false;
        } else if(pass.equals(confirm)){
            return true;
        } else{
            return false;
        }
    }

    /**
     * making sure the email is what is saved in the backend
     * @param email
     * @return
     */
    private boolean isEmailValid(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    @Override
    public void onRegisterSuccess(int id){
        // On Success of registering user, get user profile
        GetUserTask getUserTask = new GetUserTask(id, this, (MainApp)this.getApplication());
        getUserTask.execute();
    }

    @Override
    public void onRegisterError(String message){
        // Display error message to user
        Toast err = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
        err.show();
        mProgress.closeDialog();
    }


    @Override
    public void onGetUserSuccess() {
        // If successfully got user profile, then go to main activity
        Logger.d(TAG, "Successfully got the user profile");
        mProgress.closeDialog();
        Intent i = new Intent(RegisterUserActivity.this, HomeTabsActivity.class);
        startActivity(i);
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
    }

    @Override
    public void onGetUserError(String message) {
        // Display error message to user
        Toast err = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
        err.show();
        mProgress.closeDialog();
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        //closing transition animations
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
        mProgress.closeDialog();
    }

    // 2.0 and above
    @Override
    public void onBackPressed() {
        animateAndClose();
    }

    public void animateAndClose(){
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
        mProgress.closeDialog();
        finish();
    }

    // Before 2.0
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            animateAndClose();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

}



