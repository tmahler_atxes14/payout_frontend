/**
 * Bets List Adapter
 *
 * @desc - Adapter that contains the list of bets
 * @authors - Tim Mahler, Nick Burrin, Ryan Templin
 * @version - 1.0.0
 */

package com.example.ryantemplin.payoutapp.adapters;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.ryantemplin.payoutapp.R;
import com.example.ryantemplin.payoutapp.pojos.Active;
import com.example.ryantemplin.payoutapp.pojos.Bet;
import com.example.ryantemplin.payoutapp.pojos.Completed;
import com.example.ryantemplin.payoutapp.pojos.Future;
import com.example.ryantemplin.payoutapp.pojos.Game;
import com.example.ryantemplin.payoutapp.pojos.GameBet;
import com.example.ryantemplin.payoutapp.pojos.Status;
import com.example.ryantemplin.payoutapp.pojos.Team;
import com.example.ryantemplin.payoutapp.util.DataStore;
import com.example.ryantemplin.payoutapp.util.ImageUtil;
import com.example.ryantemplin.payoutapp.util.Logger;
import com.example.ryantemplin.payoutapp.util.MainApp;
import com.example.ryantemplin.payoutapp.util.MoneyUtil;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


public class BetsListAdapter extends ArrayAdapter<Object> {
    private static final String TAG = BetsListAdapter.class.getSimpleName();

    private ArrayList<Object> betsList;
    private MainApp application;

    /**
     * A container for a specific deal in the ListView.
     */
    static class ViewHolder {
        ImageView homeTeamLogoView;
        ImageView awayTeamLogoView;
        TextView homeTeamName;
        TextView awayTeamName;
        TextView homeTeamRecord;
        TextView awayTeamRecord;
        TextView extra;
        TextView timeView;
        TextView timeHeader;
        TextView betLosings;
        TextView betWinnings;
        FrameLayout betWinningsLayout;
        FrameLayout betLosingsLayout;
        TextView homeRunsScored;
        TextView awayRunsScored;
    }

    public BetsListAdapter(Context context, ArrayList<Object> betsList, MainApp app) {
        super(context, 0, betsList);
        this.betsList = betsList;
        this.application = app;

        Logger.d(TAG, "games size = " + betsList.size());

        for(int i = 0; i < betsList.size(); i++){
            Logger.d(TAG, betsList.get(i).toString());
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){

        ViewHolder holder = null;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            holder = new ViewHolder();

            //Get whatever is @ position
            Object item = getItem(position);
            int itemPosition = getItemViewType(position);

            // Game object in array
            if (itemPosition < 3) {
                // Not Header, bet object
                GameBet bet = (GameBet) item;
                Game game = DataStore.getInstance().getGameHash().get(bet.getGameID());

                Team homeTeam = game.getHomeTeam();
                Team awayTeam = game.getAwayTeam();

                if(itemPosition <= 1) {
                    // Future or Active Bet
                    // Calendar now = Calendar.getInstance();
                    if(itemPosition == 0){
                        convertView = inflater.inflate(R.layout.list_item_bet_future, parent, false);
                    }else {
                        convertView = inflater.inflate(R.layout.list_item_game, parent, false);
                    }

                    holder.homeTeamLogoView = (ImageView) convertView.findViewById(R.id.iv_home_team_logo);
                    holder.awayTeamLogoView = (ImageView) convertView.findViewById(R.id.iv_away_team_logo);

                    // Home team views
                    holder.homeTeamRecord = (TextView) convertView.findViewById(R.id.tv_home_team_record);

                    // Away team views
                    holder.awayTeamRecord = (TextView) convertView.findViewById(R.id.tv_away_team_record);

                    // Set time view
                    if(itemPosition == 0) {
                        holder.timeView = (TextView) convertView.findViewById(R.id.tv_bet_time);
                    }else{
                        holder.homeTeamName = (TextView) convertView.findViewById(R.id.tv_home_team_name);
                        holder.awayTeamName = (TextView) convertView.findViewById(R.id.tv_away_team_name);
                    }
                }else {
                    // Completed bet
                    convertView = inflater.inflate(R.layout.list_item_bet_completed, parent, false);
                    holder.homeTeamLogoView = (ImageView) convertView.findViewById(R.id.iv_home_team_logo);
                    holder.awayTeamLogoView = (ImageView) convertView.findViewById(R.id.iv_away_team_logo);
                    holder.betWinnings = (TextView) convertView.findViewById(R.id.tv_bet_winnings);
                    holder.betLosings = (TextView) convertView.findViewById(R.id.tv_bet_losings);
                    holder.betWinningsLayout = (FrameLayout) convertView.findViewById(R.id.fl_bet_winnings);
                    holder.betLosingsLayout = (FrameLayout) convertView.findViewById(R.id.fl_bet_losings);
                    holder.homeRunsScored = (TextView) convertView.findViewById(R.id.tv_home_team_scored);
                    holder.awayRunsScored = (TextView) convertView.findViewById(R.id.tv_away_team_scored);
                }

            }else{
                // Label object in array
                convertView = inflater.inflate(R.layout.list_item_time, parent, false);
                holder.timeHeader = (TextView) convertView.findViewById(R.id.tv_time_header);
            }

            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Object item = getItem(position);
        int itemPosition = getItemViewType(position);

        if (itemPosition < 3) {
            // Game Bet
            GameBet bet = (GameBet) item;
            Game game = DataStore.getInstance().getGameHash().get(bet.getGameID());

            Team homeTeam = game.getHomeTeam();
            Team awayTeam = game.getAwayTeam();

            Logger.d(TAG, "Home Team:"+homeTeam +", postition:"+itemPosition);
            Logger.d(TAG, "Away Team:"+awayTeam+", postition:"+itemPosition);

            ImageUtil.displayHighResImage(holder.homeTeamLogoView, homeTeam.getLogoUrl());
            ImageUtil.displayHighResImage(holder.awayTeamLogoView, awayTeam.getLogoUrl());

            if (itemPosition <= 1) {
                // Future Bet
                holder.homeTeamRecord.setText(homeTeam.getRecord());

                holder.awayTeamRecord.setText(awayTeam.getRecord());

                if(itemPosition == 0){
                    holder.timeView.setText(game.getDate_string());
                }else{
                    holder.awayTeamName.setText(awayTeam.getTeamName());
                    holder.homeTeamName.setText(homeTeam.getTeamName());
                }
            }else{
                // Completed Bet
                Completed betStatus = (Completed) bet.getStatus();
                Integer totalAmountSpent = bet.getTotalAmountSpent();
                Integer totalAmountWon = betStatus.getGameWinnings();
                Integer difference = totalAmountSpent - totalAmountWon;
                double diffFloat = (difference / 100.0);

                // Check to see if won or lost
                if(diffFloat >= 0){
                    holder.betLosingsLayout.setVisibility(LinearLayout.VISIBLE);
                    holder.betWinningsLayout.setVisibility(LinearLayout.GONE);
                    holder.betLosings.setText(String.format("- %.2f",diffFloat));
                }else {
                    diffFloat = diffFloat*-1;
                    holder.betLosingsLayout.setVisibility(LinearLayout.GONE);
                    holder.betWinningsLayout.setVisibility(LinearLayout.VISIBLE);
                    holder.betWinnings.setText(String.format("+ %.2f",diffFloat));
                }

                holder.homeRunsScored.setText(String.valueOf(betStatus.getHomeRuns()));
                holder.awayRunsScored.setText(String.valueOf(betStatus.getAwayRuns()));

            }

        }else{
            // Set time holder
            holder.timeHeader.setText((String) item);
        }

        return convertView;
    }

    @Override
    public int getViewTypeCount() {
        // we have two view types, GameBets and times
        return 4;
    }

    @Override
    public int getCount() {
        return betsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        Object item = null;
        if (position < betsList.size())
            item = betsList.get(position);

        if (item != null && item instanceof GameBet){
            GameBet game = (GameBet) item;
            Status gameStatus = game.getStatus();
            if(gameStatus instanceof Future){
                return 0;
            }else if(gameStatus instanceof Active){
                return 1;
            }else {
                // Completed bet
                return 2;
            }
        }
        else {
            return 3; // Status String
        }
    }

    @Override
    public Object getItem(int position) {
        Object item = null;
        if (position < betsList.size())
            item = betsList.get(position);
        return item;
    }
}
