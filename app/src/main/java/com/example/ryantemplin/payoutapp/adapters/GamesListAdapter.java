package com.example.ryantemplin.payoutapp.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ryantemplin.payoutapp.R;
import com.example.ryantemplin.payoutapp.pojos.Game;
import com.example.ryantemplin.payoutapp.pojos.Team;
import com.example.ryantemplin.payoutapp.util.DataStore;
import com.example.ryantemplin.payoutapp.util.Logger;
import com.example.ryantemplin.payoutapp.util.MainApp;

import com.example.ryantemplin.payoutapp.util.ImageUtil;

import java.util.ArrayList;

/**
 * Created by tim-azul on 4/15/15.
 */
public class GamesListAdapter extends ArrayAdapter<Object>{
    private static final String TAG = GamesListAdapter.class.getSimpleName();

    private ArrayList<Object> gamesList;
    private MainApp application;

    /**
     * A container for a specific deal in the ListView.
     */
    static class ViewHolder {
        ImageView homeTeamLogoView;
        ImageView awayTeamLogoView;
        TextView homeTeamName;
        TextView awayTeamName;
        TextView homeTeamRecord;
        TextView awayTeamRecord;
        TextView timeHeader;
        ImageView chipView;
    }

    public GamesListAdapter(Context context, ArrayList<Object> gamesList, MainApp app) {
        super(context, 0, gamesList);
        this.gamesList = gamesList;
        this.application = app;

        Logger.d(TAG, "games size = " + gamesList.size());
        for(int i = 0; i < gamesList.size(); i++){
            Logger.d(TAG, gamesList.get(i).toString());
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            holder = new ViewHolder();
            if (getItemViewType(position) == 0) {
                // Game object in array
                convertView = inflater.inflate(R.layout.list_item_game, parent, false);

                // Home team views
                holder.homeTeamLogoView = (ImageView) convertView.findViewById(R.id.iv_home_team_logo);
                holder.homeTeamName = (TextView) convertView.findViewById(R.id.tv_home_team_name);
                holder.homeTeamRecord = (TextView) convertView.findViewById(R.id.tv_home_team_record);

                // Away team views
                holder.awayTeamLogoView = (ImageView) convertView.findViewById(R.id.iv_away_team_logo);
                holder.awayTeamName = (TextView) convertView.findViewById(R.id.tv_away_team_name);
                holder.awayTeamRecord = (TextView) convertView.findViewById(R.id.tv_away_team_record);

                holder.chipView = (ImageView) convertView.findViewById(R.id.iv_chip_glyph);


            } else {
                // Time object in array
                convertView = inflater.inflate(R.layout.list_item_time, parent, false);
                holder.timeHeader = (TextView) convertView.findViewById(R.id.tv_time_header);

            }
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Object item = getItem(position);

        if (getItemViewType(position) == 0) {
            Game game = (Game)item;
            Team homeTeam = game.getHomeTeam();
            Team awayTeam = game.getAwayTeam();
            String gameId = game.getData_id();

            //put the team name
            holder.homeTeamName.setText(homeTeam.getTeamName());
            holder.awayTeamName.setText(awayTeam.getTeamName());
            holder.homeTeamRecord.setText(homeTeam.getRecord());
            holder.awayTeamRecord.setText(awayTeam.getRecord());
            ImageUtil.displayHighResImage(holder.homeTeamLogoView, homeTeam.getLogoUrl());
            ImageUtil.displayHighResImage(holder.awayTeamLogoView, awayTeam.getLogoUrl());


            if(DataStore.getInstance().getUser().getFutureBetsHash().containsKey(gameId) || DataStore.getInstance().getUser().getActiveBetsHash().containsKey(gameId)){
                holder.chipView.setVisibility(View.VISIBLE);
            }else{
                holder.chipView.setVisibility(View.GONE);
            }
        }else{
            // Set time holder
            holder.timeHeader.setText((String) item);
        }

        return convertView;
    }

    @Override
    public int getViewTypeCount() {
        // we have two view types, games and times
        return 2;
    }

    @Override
    public int getCount() {
        return gamesList.size();
    }

    @Override
    public int getItemViewType(int position) {
        Object item = null;
        if (position < gamesList.size())
            item = gamesList.get(position);

        if (item != null && item instanceof Game)
            return 0; // Game object
        else
            return 1; // Time String
    }

    @Override
    public Object getItem(int position) {
        Object item = null;
        if (position < gamesList.size())
            item = gamesList.get(position);
        return item;
    }

//    private void populateGameHolder(final Game game, ViewHolder holder) {
//        ImageUtils.displayLowResImage(holder.imageView, game);
//
//    }

}
