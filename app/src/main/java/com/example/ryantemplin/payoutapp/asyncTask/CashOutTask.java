/**
 * Cash Out Task
 *
 * @desc - task that posts the cashing out of money for a user if they have > 100
 * @authors - Tim Mahler, Nick Burrin, Ryan Templin
 * @version - 1.0.0
 */

package com.example.ryantemplin.payoutapp.asyncTask;

import android.os.AsyncTask;


import com.example.ryantemplin.payoutapp.util.DataStore;
import com.example.ryantemplin.payoutapp.util.Internet;
import com.example.ryantemplin.payoutapp.util.Logger;
import com.example.ryantemplin.payoutapp.util.MainApp;
import com.example.ryantemplin.payoutapp.util.Server;


import org.json.JSONException;
import org.json.JSONObject;


public class CashOutTask extends AsyncTask<String, Void, String> {

    private static final String TAG = CashOutTask.class.getSimpleName();

    private OnCashOutListener callback;
    private MainApp application;
    private JSONObject cashoutObj;
    private String url;
    private Integer amount;

    private String mName;
    private String mAddress;
    private String mAreaCode;
    private String mCity;
    private String mState;
    private Integer mAmount;

    public interface OnCashOutListener {
        public void OnCashoutSuccess();
        public void OnCashoutError(String message);
    }

    public CashOutTask(OnCashOutListener callback, MainApp app,
                        String name, String address, String areaCode, String city, String state, Integer amount) {
        this.callback = callback;
        this.application = app;
        this.cashoutObj = new JSONObject();
        this.amount = amount;

        // Set class attrs
        this.mName = name;
        this.mAddress = address;
        this.mAreaCode = areaCode;
        this.mCity = city;
        this.mState = state;
        this.mAmount = amount;
    }

    @Override
    protected void onPreExecute(){
        url = Server.URL_USERS + "/" + DataStore.getInstance().getUser().getUserId() + "/cash_out";
        //need to add user id
        try{
            cashoutObj.put("full_name", mName);
            cashoutObj.put("address", mAddress);
            cashoutObj.put("zip_code", mAreaCode);
            cashoutObj.put("city", mCity);
            cashoutObj.put("state", mState);
            cashoutObj.put("amount", mAmount);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected String doInBackground(String... strings) {
        return Internet.postToURl(url, cashoutObj);
    }

    @Override
    protected void onPostExecute(String result) {
        // Parse the json
        // Determine if success or error
        // Handle appropriatley
        Logger.d(TAG, "RESULTS: " + result.toString());
        if(result.equals(Internet.INTERNET_FAILED)){
            // No connection or something along those lines
            if(callback != null){
                callback.OnCashoutError(Internet.INTERNET_FAILED_MESSAGE);
            }
            return;
        }

        try {
            JSONObject resultObj = new JSONObject(result);
            Logger.d(TAG, resultObj.toString());

            if(resultObj.has("err") || resultObj.has("error")){
                callback.OnCashoutError(resultObj.getString("error"));
            } else{
                // Return to activity
                callback.OnCashoutSuccess();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
