/**
 * Create Bets Task
 *
 * @desc - task that creates a bet
 * @authors - Tim Mahler, Nick Burrin, Ryan Templin
 * @version - 1.0.0
 */


package com.example.ryantemplin.payoutapp.asyncTask;

import android.os.AsyncTask;

import com.example.ryantemplin.payoutapp.pojos.Bet;
import com.example.ryantemplin.payoutapp.pojos.GameBet;
import com.example.ryantemplin.payoutapp.pojos.Team;
import com.example.ryantemplin.payoutapp.pojos.User;
import com.example.ryantemplin.payoutapp.util.DataStore;
import com.example.ryantemplin.payoutapp.util.Internet;
import com.example.ryantemplin.payoutapp.util.Logger;
import com.example.ryantemplin.payoutapp.util.MainApp;
import com.example.ryantemplin.payoutapp.util.Server;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;

/**
 * Created by nickburrin on 4/21/15.
 */
public class CreateBetsTask extends AsyncTask<String, Void, String> {

    private static final String TAG = CreateBetsTask.class.getSimpleName();

    //private final String date;
    private String url;
    private OnCreateBetsListener callback;
    private MainApp application;
    private JSONObject overUnderBetObj;
    private JSONObject winnerBetObj;
    private JSONObject homeSuperStarBetObj;
    private JSONObject awaySuperStarBetObj;
    private JSONObject gameBetObj;


    private GameBet myGameBet;

    public interface OnCreateBetsListener {
        public void OnCreateBetsSuccess();
        public void OnCreateBetsError(String message);
    }

    public CreateBetsTask(GameBet myGameBet, OnCreateBetsListener callback, MainApp application) {
        this.callback = callback;
        this.application = application;
        this.myGameBet = myGameBet;
        this.overUnderBetObj = new JSONObject();
        this.winnerBetObj = new JSONObject();
        this.homeSuperStarBetObj = new JSONObject();
        this.awaySuperStarBetObj = new JSONObject();
        this.gameBetObj = new JSONObject();

    }


    @Override
    protected void onPreExecute(){
        int user_id = DataStore.getInstance().getUser().getUserId();
        String stringUserId = Integer.toString(user_id);
        //change the url to include the user id and game id
        String userCreateBets = Server.URL_CREATE_BETS.replaceAll(":u_id", stringUserId);
        String createBets = userCreateBets.replaceAll(":g_id", myGameBet.getGame_id());
        url = createBets;
        //need to add user id

        try {
            //put values in OverUnder bet JSON Object
            if (myGameBet.getOub() != null && myGameBet.getOub().getAmount() > 0) {
                overUnderBetObj.put("amount", myGameBet.getOub().getAmount());
                overUnderBetObj.put("over_under", myGameBet.getOub().getOverOrUnder());
                gameBetObj.put("over_under", overUnderBetObj);
            }

            //put values in WinnerLine bet JSON Object
            if (myGameBet.getLwb() != null && myGameBet.getLwb().getAmount() > 0) {
                winnerBetObj.put("amount", myGameBet.getLwb().getAmount());
                winnerBetObj.put("team_id", myGameBet.getLwb().getChoiceTeamId());
                gameBetObj.put("winner_line", winnerBetObj);
            }

            //put values in Home SuperStar bet JSON Object
            if (myGameBet.getHomeSuperStarBet() != null && myGameBet.getHomeSuperStarBet().getAmount() > 0) {
                homeSuperStarBetObj.put("amount", myGameBet.getHomeSuperStarBet().getAmount());
                homeSuperStarBetObj.put("id", myGameBet.getHomeSuperStarBet().getPlayerId());
                homeSuperStarBetObj.put("will_hit_hr", myGameBet.getHomeSuperStarBet().getHR());
                gameBetObj.put("home_superstar", homeSuperStarBetObj);
            }

            //put values in Away SuperStar bet JSON Object
            if (myGameBet.getAwaySuperStarBet() != null && myGameBet.getAwaySuperStarBet().getAmount() > 0) {
                awaySuperStarBetObj.put("amount", myGameBet.getAwaySuperStarBet().getAmount());
                awaySuperStarBetObj.put("id", myGameBet.getAwaySuperStarBet().getPlayerId());
                awaySuperStarBetObj.put("will_hit_hr", myGameBet.getAwaySuperStarBet().getHR());
                gameBetObj.put("away_superstar", awaySuperStarBetObj);
            }

        } catch (JSONException e) {
           e.printStackTrace();
        }



    }

    @Override
    protected String doInBackground(String... params) {
        return Internet.postToURl(url, gameBetObj);
    }

    @Override
    protected void onPostExecute(String result) {
        // Parse the json
        // Determine if success or error
        // Handle appropriatley
        Logger.d(TAG, "RESULTS: " + result.toString());
        if(result.equals(Internet.INTERNET_FAILED)){
            // No connection or something along those lines
            if(callback != null){
                //callback.OnGetGamesError(Internet.INTERNET_FAILED_MESSAGE);
            }
            return;
        }
        try {
            JSONObject resultObj = new JSONObject(result);
            Logger.d(TAG, resultObj.toString());
            if(resultObj.has("err") || resultObj.has("error")){
                callback.OnCreateBetsError(resultObj.getString("error"));
            }else{
                // Successfully got games
                // Return to activity
                JSONObject resultStringObj = resultObj.getJSONObject("results");
                int userGameBetId = resultStringObj.getInt("user_game_bet_id");//winner_line_id
                callback.OnCreateBetsSuccess();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
