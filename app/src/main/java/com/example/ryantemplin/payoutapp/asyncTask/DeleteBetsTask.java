/**
 * Delete Bets Task
 *
 * @desc - task that calls the endpoint to delete a bet
 * @authors - Tim Mahler, Nick Burrin, Ryan Templin
 * @version - 1.0.0
 */

package com.example.ryantemplin.payoutapp.asyncTask;

import android.os.AsyncTask;

import com.example.ryantemplin.payoutapp.util.DataStore;
import com.example.ryantemplin.payoutapp.util.Internet;
import com.example.ryantemplin.payoutapp.util.Logger;
import com.example.ryantemplin.payoutapp.util.MainApp;
import com.example.ryantemplin.payoutapp.util.Server;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ryantemplin on 5/6/15.
 */
public class DeleteBetsTask extends AsyncTask<String, Void, String> {

    private static final String TAG = DeleteBetsTask.class.getSimpleName();

    private String url;
    private String gameId;
    private OnDeleteBetsListener callback;
    private MainApp application;


    public interface OnDeleteBetsListener {
        public void OnDeleteBetsSuccess();
        public void OnDeleteBetsError(String message);
    }

    public DeleteBetsTask(String gameId, OnDeleteBetsListener callback, MainApp application) {
        this.callback = callback;
        this.application = application;
        this.gameId = gameId;
    }


    @Override
    protected void onPreExecute(){
        int user_id = DataStore.getInstance().getUser().getUserId();
        String stringUserId = Integer.toString(user_id);

        String userDeleteBets = Server.URL_DELETE_BETS.replaceAll(":u_id", stringUserId);
        String deleteBets = userDeleteBets.replaceAll(":g_id", gameId);

        url = deleteBets;
        //need to add user id



    }

    @Override
    protected String doInBackground(String... params) {
        return Internet.postToURl(url,null);
    }

    @Override
    protected void onPostExecute(String result) {
        // Parse the json
        // Determine if success or error
        // Handle appropriatley
        Logger.d(TAG, "RESULTS: " + result.toString());
        if(result.equals(Internet.INTERNET_FAILED)){
            // No connection or something along those lines
            if(callback != null){
                //callback.OnGetGamesError(Internet.INTERNET_FAILED_MESSAGE);
            }
            return;
        }

        try {
            JSONObject resultObj = new JSONObject(result);
            Logger.d(TAG, resultObj.toString());
            if(resultObj.has("err") || resultObj.has("error")){
                callback.OnDeleteBetsError(resultObj.getString("error"));
            }else{
                // Successfully got games
                // Return to activity

                callback.OnDeleteBetsSuccess();

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
