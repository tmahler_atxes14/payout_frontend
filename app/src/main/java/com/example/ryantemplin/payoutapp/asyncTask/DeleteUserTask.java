package com.example.ryantemplin.payoutapp.asyncTask;

import android.os.AsyncTask;

import com.example.ryantemplin.payoutapp.util.Internet;
import com.example.ryantemplin.payoutapp.util.Logger;
import com.example.ryantemplin.payoutapp.util.MainApp;
import com.example.ryantemplin.payoutapp.util.Server;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by nickburrin on 5/9/15.
 */
public class DeleteUserTask extends AsyncTask<String, Void, String> {

    private static final String TAG = DeleteBetsTask.class.getSimpleName();

    //private final String date;
    private String url;
    private OnDeleteUserListener callback;
    private MainApp application;

    public interface OnDeleteUserListener {
        public void OnDeleteUserSuccess();
        public void OnDeleteUserError(String message);
    }

    public DeleteUserTask(String userId, OnDeleteUserListener callback, MainApp application){
        this.url = Server.URL_DELETE_USER;
        this.callback = callback;
        this.application = application;

        url = url.replace(":u_id", userId);
    }

    @Override
    protected String doInBackground(String... strings) {
        return Internet.postToURl(url, null);
    }

    @Override
    protected void onPostExecute(String result) {
        // Parse the json
        // Determine if success or error
        // Handle appropriatley
        Logger.d(TAG, "RESULTS: " + result.toString());
        if(result.equals(Internet.INTERNET_FAILED)){
            // No connection or something along those lines
            if(callback != null){
                callback.OnDeleteUserError(Internet.INTERNET_FAILED_MESSAGE);
            }
            return;
        }

        try {
            JSONObject resultObj = new JSONObject(result);
            Logger.d(TAG, resultObj.toString());
            if(resultObj.has("err") || resultObj.has("error")){
                callback.OnDeleteUserError(resultObj.getString("error"));
            }else{
                // Return to activity
                callback.OnDeleteUserSuccess();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
