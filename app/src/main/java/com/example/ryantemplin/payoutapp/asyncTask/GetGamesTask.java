/**
 * GetGamesTask gets the list of games from the backend
 *
 * @desc - task that calls the endpoint to delete a bet
 * @authors - Tim Mahler, Nick Burrin, Ryan Templin
 * @version - 1.0.0
 */

package com.example.ryantemplin.payoutapp.asyncTask;

import android.os.AsyncTask;

import com.example.ryantemplin.payoutapp.pojos.Game;
import com.example.ryantemplin.payoutapp.pojos.Team;
import com.example.ryantemplin.payoutapp.util.DataStore;
import com.example.ryantemplin.payoutapp.util.Internet;
import com.example.ryantemplin.payoutapp.util.Logger;
import com.example.ryantemplin.payoutapp.util.MainApp;
import com.example.ryantemplin.payoutapp.util.Server;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;

public class GetGamesTask extends AsyncTask<String, Void, String> {

    private static final String TAG = GetGamesTask.class.getSimpleName();

    private final String date;
    private String url;
    private OnGetGamesListener callback;
    private MainApp application;
    private JSONObject gameObj;

    public interface OnGetGamesListener {
        public void OnGetGamesSuccess();
        public void OnGetGamesError(String message);
    }

    public GetGamesTask(OnGetGamesListener callback, MainApp application) {
        this.callback = callback;
        this.application = application;
        this.gameObj = new JSONObject();
        this.date = null;
    }

    @Override
    protected void onPreExecute(){
        url = Server.URL_GET_GAMES;
        //need to add user id
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZ");
            Calendar cal = Calendar.getInstance();
            String time = dateFormat.format(cal.getTime());
            gameObj.put("date", time);//"2015-04-07T10:49:32.633Z"
            Logger.d(TAG, gameObj.toString());
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected String doInBackground(String... params) {
        return Internet.postToURl(url, gameObj);
    }

    @Override
    protected void onPostExecute(String result) {
        // Parse the json
        // Determine if success or error
        // Handle appropriatley
        Logger.d(TAG, "RESULTS: " + result.toString());
        if(result.equals(Internet.INTERNET_FAILED)){
            // No connection or something along those lines
            if(callback != null){
                callback.OnGetGamesError(Internet.INTERNET_FAILED_MESSAGE);
            }
            return;
        }

        try {
            JSONObject resultObj = new JSONObject(result);
            Logger.d(TAG, resultObj.toString());
            if(resultObj.has("err") || resultObj.has("error")){
                callback.OnGetGamesError(resultObj.getString("error"));
            }else{
                // Successfully got games
                JSONArray gamesJSONArray = resultObj.getJSONArray("results");
                // Create new user from params

                ArrayList<Game> gameArray = new ArrayList<Game>();
                HashMap<String, Game> gameHash = new HashMap<String, Game>(); // HashMap of Game object (key == id)

                HashMap<String, Team> teamsHash = new HashMap<String, Team>();

                //for every game, get the information for each game by parsing out the JSON objects
                for (int i = 0; i < gamesJSONArray.length(); i++){
                    // For each json object in the results array, parse the game object and create new Game class
                    JSONObject gameObj = gamesJSONArray.getJSONObject(i);
                    String gameId = gameObj.getString("data_id");

                    //get the teams JSON and the embedded home teams within the team JSON
                    JSONObject teamsObj = gameObj.getJSONObject("teams");
                    JSONObject homeTeamsObj = teamsObj.getJSONObject("home");

                    //get the awayteam JSON object from team JSON
                    JSONObject awayTeamsObj = teamsObj.getJSONObject("away");

                    // Create teams
                    String homeTeamId = homeTeamsObj.getString("data_id");
                    String awayTeamId = awayTeamsObj.getString("data_id");

                    Team homeTeam = null;
                    Team awayTeam = null;

                    if(teamsHash.containsKey(homeTeamId)){
                        homeTeam = teamsHash.get(homeTeamId);
                    }else{
                        homeTeam = new Team(homeTeamsObj);
                        teamsHash.put(homeTeamId, homeTeam);
                    }

                    if(teamsHash.containsKey(awayTeamId)){
                        awayTeam = teamsHash.get(awayTeamId);
                    }else{
                        awayTeam = new Team(awayTeamsObj);
                        teamsHash.put(awayTeamId, awayTeam);
                    };


                    Game newGame = new Game(gameObj, homeTeam, awayTeam);

                    // Add to game array
                    gameArray.add(newGame);

                    // Add to game hash
                    gameHash.put(gameId, newGame);

                }

                Collections.sort(gameArray, new Game.GameComparator());

                // Print games list
                Logger.d(TAG, "games size = "+gameArray.size());
                for(int i = 0; i < gameArray.size(); i++){
                    Logger.d(TAG, gameArray.get(i).toString());
                }

                // Store in the datastore
                DataStore.getInstance().setGameArray(gameArray);
                DataStore.getInstance().setGameHash(gameHash);
                DataStore.getInstance().setTeamsHash(teamsHash);

                // Return to activity
                callback.OnGetGamesSuccess();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}