/**
 * Get User gets user profile from the backend via HTTP
 *
 * @desc - task that calls the endpoint to get a user profile
 * @authors - Tim Mahler, Nick Burrin, Ryan Templin
 * @version - 1.0.0
 */

package com.example.ryantemplin.payoutapp.asyncTask;

import android.os.AsyncTask;

import com.example.ryantemplin.payoutapp.pojos.Active;
import com.example.ryantemplin.payoutapp.pojos.Completed;
import com.example.ryantemplin.payoutapp.pojos.Future;
import com.example.ryantemplin.payoutapp.pojos.Game;
import com.example.ryantemplin.payoutapp.pojos.GameBet;
import com.example.ryantemplin.payoutapp.pojos.LineWinnerBet;
import com.example.ryantemplin.payoutapp.pojos.OverUnderBet;
import com.example.ryantemplin.payoutapp.pojos.SuperStar;
import com.example.ryantemplin.payoutapp.pojos.SuperStarBet;
import com.example.ryantemplin.payoutapp.pojos.Team;
import com.example.ryantemplin.payoutapp.pojos.User;
import com.example.ryantemplin.payoutapp.util.DataStore;
import com.example.ryantemplin.payoutapp.util.Internet;
import com.example.ryantemplin.payoutapp.util.Logger;
import com.example.ryantemplin.payoutapp.util.MainApp;
import com.example.ryantemplin.payoutapp.util.Server;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class GetUserTask extends AsyncTask<String, Void, String> {

    private static final String TAG = RegisterUserTask.class.getSimpleName();

    private final int mUserId;
    private String url;
    private OnGetUserListener callback;
    private MainApp application;


    public interface OnGetUserListener {
        public void onGetUserSuccess();
        public void onGetUserError(String message);
    }

    public GetUserTask(int userId, OnGetUserListener callback, MainApp application) {
        this.mUserId = userId;
        this.callback = callback;
        this.application = application;
    }

    @Override
    protected void onPreExecute(){
        url = Server.URL_GET_USER;
        url += mUserId;
    }

    @Override
    protected String doInBackground(String... params) {
        return Internet.getFromURLWithoutAuth(url);
    }

    @Override
    protected void onPostExecute(String result) {
        // Parse the json
        // Determine if success or error
        // Handle appropriatley
        Logger.d(TAG, "RESULTS: "+result.toString());
        if(result.equals(Internet.INTERNET_FAILED)){
            // No connection or something along those lines
            if(callback != null){
                callback.onGetUserError(Internet.INTERNET_FAILED_MESSAGE);
            }
            return;
        }

        try {
            JSONObject resultObj = new JSONObject(result);
            Logger.d(TAG, resultObj.toString());
            if(resultObj.has("err") || resultObj.has("error")){
                callback.onGetUserError(resultObj.getString("error"));
            }else{
                // Successfully got user profile
                JSONObject userObj = resultObj.getJSONObject("results");
                // Create new user from params
                String firstName = userObj.getString("first_name");
                String lastName = userObj.getString("last_name");
                String email = userObj.getString("email");
                int total_credit = userObj.getInt("total_credit");

                User newUser = new User(mUserId, firstName, lastName, email, total_credit);
                DataStore.getInstance().setUser(newUser);
                HashMap<String, Game> gamesHash = DataStore.getInstance().getGameHash();


                JSONObject betsObj = userObj.getJSONObject("bets");
                //get the future bets array
                if(!betsObj.isNull("future")){
                    JSONArray futureBetsArr = betsObj.getJSONArray("future");
                    HashMap<String, GameBet> futuresBetHash = new HashMap<String, GameBet>();


                    JSONObject futureBet = null;
                    GameBet futureGameBet;
                    Game game = null;
                    String game_id = "";

                    //get all users bets and update the hashmap
                    for(int i = 0; i < futureBetsArr.length(); i++){
                        futureBet = futureBetsArr.getJSONObject(i);
                        game_id = futureBet.getString("game_id");

                        // Get game
                        if(!gamesHash.containsKey(game_id)){
                            // Parse and create game
                            game = parseGame(futureBet, game_id);
                            if(game != null){
                                gamesHash.put(game_id, game);
                            }
                        }

                        futureGameBet = parseGameBet(futureBet, "future");
                        futuresBetHash.put(game_id, futureGameBet);
                    }

                    DataStore.getInstance().getUser().setFutureBetsHash(futuresBetHash);
                }
                //get the active bets array
                if(!betsObj.isNull("active")){
                    JSONArray activeBetsArr = betsObj.getJSONArray("active");
                    HashMap<String, GameBet> activesBetHash = new HashMap<String, GameBet>();

                    JSONObject activeBet = null;
                    GameBet activeGameBet;
                    Game game = null;
                    String game_id = "";

                    for(int i = 0; i < activeBetsArr.length(); i++){
                        activeBet = activeBetsArr.getJSONObject(i);
                        game_id = activeBet.getString("game_id");

                        // Get game
                        if(!gamesHash.containsKey(game_id)){
                            // Parse and create game
                            game = parseGame(activeBet, game_id);
                            if(game != null){
                                gamesHash.put(game_id, game);
                            }
                        }

                        // Add to hash map
                        activeGameBet = parseGameBet(activeBet, "active");
                        activesBetHash.put(game_id, activeGameBet);
                    }

                    DataStore.getInstance().getUser().setActiveBetsHash(activesBetHash);
                }
                //get the completed bets array
                if(!betsObj.isNull("completed")){
                    JSONArray completedBetsArr = betsObj.getJSONArray("completed");
                    HashMap<String, GameBet> completedBetsHash = new HashMap<String, GameBet>();

                    JSONObject completedBet = null;
                    GameBet completedGameBet;
                    String game_id = "";
                    Game game = null;


                    for(int i = 0; i < completedBetsArr.length(); i++){
                        completedBet = completedBetsArr.getJSONObject(i);
                        game_id = completedBet.getString("game_id");

                        // Get game
                        if(!gamesHash.containsKey(game_id)){
                            // Parse and create game
                            game = parseGame(completedBet, game_id);
                            if(game != null){
                                gamesHash.put(game_id, game);
                            }
                        }

                        completedGameBet = parseGameBet(completedBet, "completed");
                        game_id = completedBet.getString("game_id");
                        completedBetsHash.put(game_id, completedGameBet);
                    }

                    DataStore.getInstance().getUser().setCompletedBetsHash(completedBetsHash);
                }
              //

                // Log user in
                this.application.logIn(newUser);

                // Return to activity
                callback.onGetUserSuccess();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Parse the game
     *
     * @param bet
     * @return
     */
    public Game parseGame(JSONObject bet, String gameId){
        Game game = null;
        HashMap<String, Team> teamsHash = DataStore.getInstance().getTeamsHash();
        try{
            //get the teams JSON and the embedded home teams within the team JSON
            JSONObject gameObj = bet.getJSONObject("game");
            JSONObject homeTeamsObj = gameObj.getJSONObject("home_team");

            //get the awayteam JSON object from team JSON
            JSONObject awayTeamsObj = gameObj.getJSONObject("away_team");

            // Create teams
            String homeTeamId = homeTeamsObj.getString("data_id");
            String awayTeamId = awayTeamsObj.getString("data_id");

            Team homeTeam = null;
            Team awayTeam = null;

            if(teamsHash.containsKey(homeTeamId)){
                homeTeam = teamsHash.get(homeTeamId);
            }else{
                homeTeam = new Team(homeTeamsObj);
                teamsHash.put(homeTeamId, homeTeam);
            }

            if(teamsHash.containsKey(awayTeamId)){
                awayTeam = teamsHash.get(awayTeamId);
            }else {
                awayTeam = new Team(awayTeamsObj);
                teamsHash.put(awayTeamId, awayTeam);
            }

            game = new Game(gameObj, homeTeam, awayTeam);

        }catch (JSONException e) {
            e.printStackTrace();
        }

        return game;
    }

    /**
     * Parse the given bet object returned from backend
     *
     * @param bet - JSOn object to be parsed
     * @return - new GameBet object
     */
    public GameBet parseGameBet(JSONObject bet, String status){
        // Params
        String game_id = null;
        Integer totalWinnings = 0;
        Integer totalAmountSpent = 0;
        JSONObject overUnderBet = null;
        JSONObject lineBet = null;
        JSONObject homeSSBet = null;
        JSONObject awaySSBet = null;
        JSONObject oddsAwaySS = null;
        JSONObject oddsHomeSS = null;
        JSONObject superstarAwaySS = null;
        JSONObject superstarHomeSS = null;
        JSONObject homeTeamObj = null;
        JSONObject awayTeamObj = null;

        JSONObject gameStats = null;
        JSONObject homeTeamStats = null;
        JSONObject awayTeamStats = null;
        Integer awayRuns = 0;
        Integer homeRuns = 0;
        GameBet result = null;

        LineWinnerBet lineBetClass = null;
        OverUnderBet overUnderBetClass = null;
        SuperStarBet homeSSBetClass = null;
        SuperStarBet awaySSBetClass = null;

        // Parse the resulting JSON object
        try {
            // Parse the keys
            game_id = bet.getString("game_id");
            totalWinnings = bet.getInt("total_winnings");
            totalAmountSpent = bet.getInt("total_amount_spent");

            overUnderBet = parseJSONKey(bet, "over_under_bet");
            lineBet =  parseJSONKey(bet, "winner_line_bet");
            homeSSBet =  parseJSONKey(bet, "superstar_home_bet");
            awaySSBet =  parseJSONKey(bet, "superstar_away_bet");
            homeTeamObj =  parseJSONKey(bet, "home_team");
            awayTeamObj =  parseJSONKey(bet, "away_team");
            gameStats = parseJSONKey(bet, "game_stats");

            //get logo urls for the home and away team
            String awayLogo =  (awayTeamObj!=null)? awayTeamObj.getString("logo_url"):null;
            String homeLogo = (homeTeamObj!=null)? homeTeamObj.getString("logo_url"):null;


            // Parse value into result bet objects
            oddsHomeSS = (homeSSBet != null)? parseJSONKey(homeSSBet, "odds") : null;
            superstarHomeSS = (homeSSBet != null)? parseJSONKey(homeSSBet, "superstar") : null;
            oddsAwaySS =  (awaySSBet != null)? parseJSONKey(awaySSBet, "odds") : null;
            superstarAwaySS = (awaySSBet != null)? parseJSONKey(awaySSBet, "superstar") : null;

            homeTeamStats = (gameStats != null)? parseJSONKey(gameStats, "home_team") : null;
            awayTeamStats = (gameStats != null)? parseJSONKey(gameStats, "away_team") : null;

            homeRuns = (homeTeamStats != null)? homeTeamStats.getInt("num_runs_scored") : 0;
            awayRuns = (awayTeamStats != null)? awayTeamStats.getInt("num_runs_scored") : 0;

            // Create bet objects given parsed info
            lineBetClass = (lineBet != null)? new LineWinnerBet(game_id, lineBet.getString("id"), lineBet.getInt("amount"), lineBet.getString("team_id")) : null;

            overUnderBetClass = (overUnderBet != null)? new OverUnderBet(game_id, overUnderBet.getString("id"), overUnderBet.getInt("amount"), overUnderBet.getInt("over_under")) : null;

            awaySSBetClass = (awaySSBet != null && oddsAwaySS!=null)? new SuperStarBet(game_id, awaySSBet.getString("id"), awaySSBet.getInt("amount"), oddsAwaySS.getDouble("odds_multiplier"),
                    superstarAwaySS.getString("data_id"), awaySSBet.getInt("will_hit_hr")) : null;

            homeSSBetClass = (homeSSBet != null && superstarHomeSS!=null)? new SuperStarBet(game_id, homeSSBet.getString("id"), homeSSBet.getInt("amount"), oddsHomeSS.getDouble("odds_multiplier"),
                    superstarHomeSS.getString("data_id"), homeSSBet.getInt("will_hit_hr")) : null;

            if (status.equals("future")) {
                result = new GameBet(game_id, new Future(), lineBetClass, overUnderBetClass, awaySSBetClass, homeSSBetClass, totalAmountSpent);
            }else if (status.equals("active")) {
                result = new GameBet(game_id, new Active(awayLogo, homeLogo), lineBetClass, overUnderBetClass, awaySSBetClass, homeSSBetClass, totalAmountSpent);
            }else if (status.equals("completed")) {
                result = new GameBet(game_id, new Completed(awayLogo, homeLogo, totalWinnings, homeRuns, awayRuns), lineBetClass, overUnderBetClass, awaySSBetClass, homeSSBetClass, totalAmountSpent);
            }



        } catch (JSONException e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     * Parse the JSON value given a key (this method was made to get around JSON parse exception being thrown)
     *
     * @param obj - JSON Object
     * @param key - String key value
     * @return JSONObject parse from key
     */
    public JSONObject parseJSONKey(JSONObject obj, String key){
        JSONObject result = null;
        try {
            if (obj.has(key) && !obj.isNull(key)) {
                result = (JSONObject) obj.get(key);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }
}
