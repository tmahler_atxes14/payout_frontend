/**
 * Login Task
 *
 * @desc - handles the HTTP request to login
 * @author- Tim Mahler
 * @version - 0.0.1
 */

package com.example.ryantemplin.payoutapp.asyncTask;

import android.os.AsyncTask;

import com.example.ryantemplin.payoutapp.util.Internet;
import com.example.ryantemplin.payoutapp.util.Logger;
import com.example.ryantemplin.payoutapp.util.MainApp;
import com.example.ryantemplin.payoutapp.util.Server;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;

public class LoginTask extends AsyncTask<String, Void, String> {

    private static final String TAG = LoginTask.class.getSimpleName();

    private final String mEmail;
    private final String mPassword;
    private OnLoginListener callback;
    private JSONObject userObj;
    private MainApp application;

    public interface OnLoginListener {
        public void onLoginSuccess(int id);
        public void onLoginError(String message);
    }

    public LoginTask(String email, String password, OnLoginListener callback) {
        this.mEmail = email;
        this.mPassword = password;
        this.userObj = new JSONObject();
        this.callback = callback;
    }

    @Override
    protected void onPreExecute(){
        try {
            userObj.put("email", mEmail);
            userObj.put("password", mPassword);
            Logger.d(TAG, userObj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected String doInBackground(String... params) {
        return Internet.postToURl(Server.URL_LOGIN, userObj);
    }

    @Override
    protected void onPostExecute(String result) {
        // Parse the json
        // Determine if success or error
        // Handle appropriatley
        Logger.d(TAG, "RESULTS: "+result.toString());
        if(result.equals(Internet.INTERNET_FAILED)){
            // No connection or something along those lines
            if(callback != null){
                callback.onLoginError(Internet.INTERNET_FAILED_MESSAGE);
            }
            return;
        }

        try {
            JSONObject resultObj = new JSONObject(result);
            Logger.d(TAG, resultObj.toString());
            if(resultObj.has("err") || resultObj.has("error")){
                callback.onLoginError(resultObj.getString("error"));
            }else{
                // Successfully registered
                // Create new user
                int id = resultObj.getInt("id");

                // Return to activity
                callback.onLoginSuccess(id);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
