/**
 * Register User Task, Registers a user profile from the backend via HTTP
 *
 * @desc - task that calls the endpoint to register a profile
 * @authors - Tim Mahler, Nick Burrin, Ryan Templin
 * @version - 1.0.0
 */

package com.example.ryantemplin.payoutapp.asyncTask;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;
import com.example.ryantemplin.payoutapp.util.*;

/**
 * Represents an asynchronous login/registration task used to authenticate
 * the user.
 */
public class RegisterUserTask extends AsyncTask<String, Void, String> {

    private static final String TAG = RegisterUserTask.class.getSimpleName();

    private final String firstName;
    private final String lastName;
    private final String mEmail;
    private final String mPassword;
    private OnRegisterListener callback;
    private JSONObject userObj;

    public interface OnRegisterListener {
        public void onRegisterSuccess(int id);
        public void onRegisterError(String message);
    }

    public RegisterUserTask(String first_name, String last_name, String email, String password, OnRegisterListener callback) {
        this.firstName = first_name;
        this.lastName = last_name;
        this.mEmail = email;
        this.mPassword = password;
        this.userObj = new JSONObject();
        this.callback = callback;
    }

    @Override
    protected void onPreExecute(){
        try {
            userObj.put("first_name", firstName);
            userObj.put("last_name", lastName);
            userObj.put("email", mEmail);
            userObj.put("password", mPassword);
            Logger.d(TAG, userObj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected String doInBackground(String... params) {
        return Internet.postToURl(Server.URL_REGISTER, userObj);
    }

    @Override
    protected void onPostExecute(String result) {
        // Parse the json
        // Determine if success or error
        // Handle appropriatley
        Logger.d(TAG, "RESULTS: "+result.toString());
        if(result.equals(Internet.INTERNET_FAILED)){
            // No connection or something along those lines
            if(callback != null){
                callback.onRegisterError(Internet.INTERNET_FAILED_MESSAGE);
            }
            return;
        }

        try {
            JSONObject resultObj = new JSONObject(result);
            Logger.d(TAG, resultObj.toString());
            if(resultObj.has("err") || resultObj.has("error")){
                callback.onRegisterError(resultObj.getString("error"));
            }else{
                // Successfully registered
                // Create new user
                int id = resultObj.getInt("id");
                // Return to activity
                callback.onRegisterSuccess(id);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}