package com.example.ryantemplin.payoutapp.dialog;

import android.app.DialogFragment;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.ryantemplin.payoutapp.R;

/**
 * Created by tim-azul on 4/13/15.
 */
public class PayoutCustomDialog extends DialogFragment {

    public static final String DIALOG_MESSAGE = "dialog_message";

    private String mMessage;
    private ProgressBar progress;
    private TextView mText;

    public static PayoutCustomDialog newInstance() {
        PayoutCustomDialog f = new PayoutCustomDialog();
        f.setCancelable(false);
        Bundle args = new Bundle();
        args.putString(DIALOG_MESSAGE, ""); // Can add message to dialog wheel
        f.setArguments(args);
        return f;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mMessage = getArguments().getString(DIALOG_MESSAGE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));
        getDialog().getWindow().setDimAmount(0.6f);

        View view = inflater.inflate(R.layout.dialog_fragment, container);
        mText = (TextView) view.findViewById(R.id.text1);
        mText.setText(mMessage);

        progress = (ProgressBar) view.findViewById(R.id.progress);
        int color = getResources().getColor(R.color.white);
        progress.getIndeterminateDrawable().setColorFilter(color, android.graphics.PorterDuff.Mode.MULTIPLY);

        return view;
    }

    /**
     * Closes the progress dialog
     */
    public void closeDialog() {
        if ((this != null) && this.isAdded() && this.isResumed()) {
            this.dismiss();
        }
    }

    /**
     * Takes the string param and makes it the toast message
     * @param msg
     */
    public void setText(String msg) {
        mText.setText(msg);
    }

}
