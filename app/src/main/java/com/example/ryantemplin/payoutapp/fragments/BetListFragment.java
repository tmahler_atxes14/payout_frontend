/**
 * Bet List Fragment - list of the bets, future, active, completed
 *
 * @desc - Fragment that contains the list of the usrs bets
 * @authors - Tim Mahler, Nick Burrin, Ryan Templin
 * @version - 1.0.0
 */

package com.example.ryantemplin.payoutapp.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.ryantemplin.payoutapp.R;
import com.example.ryantemplin.payoutapp.activities.BetActivity;
import com.example.ryantemplin.payoutapp.activities.HomeTabsActivity;
import com.example.ryantemplin.payoutapp.activities.LoginActivity;
import com.example.ryantemplin.payoutapp.adapters.BetsListAdapter;
import com.example.ryantemplin.payoutapp.asyncTask.CreateBetsTask;
import com.example.ryantemplin.payoutapp.asyncTask.DeleteBetsTask;
import com.example.ryantemplin.payoutapp.asyncTask.GetUserTask;
import com.example.ryantemplin.payoutapp.dialog.PayoutCustomDialog;
import com.example.ryantemplin.payoutapp.pojos.Active;
import com.example.ryantemplin.payoutapp.pojos.Completed;
import com.example.ryantemplin.payoutapp.pojos.GameBet;
import com.example.ryantemplin.payoutapp.util.DataStore;
import com.example.ryantemplin.payoutapp.util.Logger;
import com.example.ryantemplin.payoutapp.util.MainApp;
import com.example.ryantemplin.payoutapp.util.MoneyUtil;


import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;


public class BetListFragment extends HomeTabFragment
        implements GetUserTask.OnGetUserListener, DeleteBetsTask.OnDeleteBetsListener{

    private static final String TAG = BetListFragment.class.getSimpleName();

    // Progress dialog (loading wheel)
    PayoutCustomDialog mProgress = PayoutCustomDialog.newInstance();

    private TextView mMyBalanceView;
    private DeleteBetsTask mDeleteBetsTask;

    private ListView betsList;

    private String myGameId;

    private BetsListAdapter betsListAdapter;
    private Collection<GameBet> futureBets;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view =  inflater.inflate(R.layout.fragment_bets_list, container, false);

        betsList = (ListView) view.findViewById(R.id.lv_bets_list);
        betsList.setEmptyView(view.findViewById(R.id.tv_empty_list));

        mMyBalanceView = ((TextView) view.findViewById(R.id.tv_balance_amount));
        mMyBalanceView.setText(
                MoneyUtil.intToDollar(DataStore.getInstance().getUser().getCredit()));
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initFragment();
    }

    /**
     * Initializes all necessary fields, adds listeners, and starts the bidding engine.
     */
    public void initFragment() {


        GetUserTask refresh = new GetUserTask(DataStore.getInstance().getUser().getUserId(),
                this, application);
        refresh.execute();

        futureBets = new ArrayList<GameBet>();
    }


    @Override
    public void onResume() {
        super.onResume();
        mMyBalanceView.setText(
                MoneyUtil.intToDollar(DataStore.getInstance().getUser().getCredit()));

        // Notify that dataset has changed if different lengths
        if( (futureBets!=null) && (futureBets.size() != DataStore.getInstance().getUser().getFutureBetsHash().size()) ){
            initBetsArray();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mProgress.closeDialog();
    }

    /**
     * initialize the bets array
     */
    public void initBetsArray() {
        ArrayList<Object> allBets = new ArrayList<Object>();
        initFutureBets(allBets);
        initActiveBets(allBets);
        initCompletedBets(allBets);

        Logger.d(TAG, "bets list size = " + allBets.size());
        for (int i = 0; i < allBets.size(); i++) {
            Logger.d(TAG, allBets.get(i).toString());
        }

        // Set the adapter
        betsListAdapter = new BetsListAdapter(activity.getApplicationContext(),
                allBets, application);

        betsList.setAdapter(betsListAdapter);
        betsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object item = betsListAdapter.getItem(position);
                if (item instanceof GameBet) {
                    GameBet game = (GameBet) item;

                    if ( !(game.getStatus() instanceof Completed) ) {
                        // If clicked on bet, go to bet activity
                        Log.d(TAG, "Clicked on bet:" + item);

                        // Get the associated Game
                        String gameId = game.getGameID();

                        Intent i = new Intent(activity, BetActivity.class);
                        // Send bet id to the bet activity
                        i.putExtra("game_id", gameId);

                        // Start the activity
                        startActivity(i);
                    }
                }
            }
        });

        betsList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Object item = betsListAdapter.getItem(position);
                if (item instanceof GameBet) {
                    GameBet game = (GameBet) item;

                    if (!(game.getStatus() instanceof Completed) && !(game.getStatus() instanceof Active) ) {
                        // If clicked on bet, go to bet activity
                        Log.d(TAG, "Clicked on bet:" + item);

                        // Get the associated Game
                        myGameId = game.getGameID();
                        //make sure the user wants to delete the bet
                        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                        builder.setMessage("Are you sure you want to delete this bet?").setPositiveButton("Yes", dialogClickListener)
                                .setNegativeButton("No", dialogClickListener).show();

                    }
                }
                return true;
            }


        });

    }

    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which){
                case DialogInterface.BUTTON_POSITIVE:
                    //Yes button clicked
                    attemptDeleteBets(myGameId);
                    break;

                case DialogInterface.BUTTON_NEGATIVE:
                    //No button clicked
                    break;
            }
        }
    };

    /**
     * attempt delete bets starts the Delete Bets task given the game id selected
     * @param gameId - game id for the game to be deleted
     */
    private void attemptDeleteBets(String gameId){
        mDeleteBetsTask = new DeleteBetsTask(gameId, this, application);//, (MainApp)this.getActivity().getApplication());
        mDeleteBetsTask.execute();
    }

    /**
     * initFutureBets - add future bets list to the allbets list
     * @param allBets
     */
    private void initFutureBets(ArrayList<Object> allBets) {
        if(DataStore.getInstance().getUser().getFutureBetsHash() != null) {
            futureBets = new ArrayList<GameBet>(DataStore.getInstance().getUser().getFutureBetsHash().values());
            if(!futureBets.isEmpty()){ // no future bets
                allBets.add("FUTURE");
                allBets.addAll(futureBets);
            }
        }
    }

    /**
     * initActiveBets - add active bets list to the allbets list
     * @param allBets
     */
    private void initActiveBets(ArrayList<Object> allBets) {
        if(DataStore.getInstance().getUser().getActiveBetsHash() != null) {
            Collection<GameBet> bets = DataStore.getInstance().getUser().getActiveBetsHash().values();
            if(!bets.isEmpty()){// no active bets
                allBets.add("ACTIVE");
                allBets.addAll(bets);
            }
        }
    }

    /**
     * initCompletedBets - add completed bets list to the allbets list
     * @param allBets
     */
    private void initCompletedBets(ArrayList<Object> allBets) {
        if(DataStore.getInstance().getUser().getCompletedBetsHash() != null) {
            Collection<GameBet> bets = DataStore.getInstance().getUser().getCompletedBetsHash().values();
            if(!bets.isEmpty()){
                allBets.add("COMPLETED");
                allBets.addAll(bets);
            }
        }
    }

    @Override
    public void onGetUserSuccess() {
        mProgress.closeDialog();
        initBetsArray();
    }

    @Override
    public void onGetUserError(String message) {
        Logger.d(TAG, "Something went wrong with updating the user and their bets");
        mProgress.closeDialog();
    }

    @Override
    public void OnDeleteBetsSuccess() {
        int refundedMoney = DataStore.getInstance().getUser().getFutureBetsHash().get(myGameId).getTotalAmountSpent();
        DataStore.getInstance().getUser().setCredit(DataStore.getInstance().getUser().getCredit() + refundedMoney);
        DataStore.getInstance().getUser().getFutureBetsHash().remove(myGameId);
        activity.finish();
        startActivity(activity.getIntent());
//        Fragment frg = null;
//        frg = this;
//        final FragmentTransaction ft = getFragmentManager().beginTransaction();
//        ft.detach(frg);
//        ft.attach(frg);
//        ft.commit();
        //update the balance
    }

    @Override
    public void OnDeleteBetsError(String message) {

    }
}
