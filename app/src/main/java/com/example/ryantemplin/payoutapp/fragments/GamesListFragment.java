/**
 * Games List Fragment - list of the games for the next 2 days
 *
 * @desc - Fragment that contains the list of the games
 * @authors - Tim Mahler, Nick Burrin, Ryan Templin
 * @version - 1.0.0
 *
 */

package com.example.ryantemplin.payoutapp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.ryantemplin.payoutapp.R;
import com.example.ryantemplin.payoutapp.activities.BetActivity;
import com.example.ryantemplin.payoutapp.adapters.GamesListAdapter;
import com.example.ryantemplin.payoutapp.asyncTask.GetGamesTask;
import com.example.ryantemplin.payoutapp.dialog.PayoutCustomDialog;
import com.example.ryantemplin.payoutapp.pojos.Game;
import com.example.ryantemplin.payoutapp.util.DataStore;
import com.example.ryantemplin.payoutapp.util.Logger;
import com.example.ryantemplin.payoutapp.util.MoneyUtil;

import java.util.ArrayList;
import java.util.Calendar;


public class GamesListFragment extends HomeTabFragment
    implements GetGamesTask.OnGetGamesListener{

    private static final String TAG = GamesListFragment.class.getSimpleName();

    // Progress dialog (loading wheel)
    PayoutCustomDialog mProgress = PayoutCustomDialog.newInstance();

    private ListView gamesList;
    private GamesListAdapter gamesListAdapter;

    private TextView mMyBalanceView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view =  inflater.inflate(R.layout.fragment_games_list, container, false);

        //get text view for balance and set it to the right amount of credit
        mMyBalanceView = ((TextView) view.findViewById(R.id.tv_balance_amount));
        mMyBalanceView.setText(
                MoneyUtil.intToDollar(DataStore.getInstance().getUser().getCredit()));
        gamesList = (ListView) view.findViewById(R.id.lv_games_list);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mMyBalanceView.setText(
                MoneyUtil.intToDollar(DataStore.getInstance().getUser().getCredit()));

        Logger.d(TAG, "user credit equals datastore" + DataStore.getInstance().getUser().getCredit());
        Logger.d(TAG, "user credit user equals " + DataStore.getInstance().getUser().getCredit());

        // Notify that dataset has changed
        if(gamesListAdapter != null){
            gamesListAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mProgress.closeDialog();
    }

    /**
     * Initializes all necessary fields, adds listeners, and starts the bidding engine.
     */
    public void initFragment() {

        // condition check to retrieve list of games
        if(DataStore.getInstance().getGameArray().size() == 0){

            mProgress.show(activity.getFragmentManager(), "dialog");
            GetGamesTask getGamesTask = new GetGamesTask(this, application);
            getGamesTask.execute();
        }else{
            initGamesArray();
        }
    }

    /**
     * init games array - get the games for today and tomorrow
     */
    public void initGamesArray() {
        ArrayList<Object> games = new ArrayList<Object>();
        ArrayList<Game> gameArray = DataStore.getInstance().getGameArray();

        // For each game, add a time section header
        String currentSectionHeader = ""; // Init string
        Calendar today = Calendar.getInstance();
        for (Game game : gameArray){//every game in the game array
            String dateString = game.getDate_string();
            Calendar date = game.getDate();
            System.out.println("date =" + date);
            if(!currentSectionHeader.equals(dateString)){
                // Check to see if need to add "Tommorow"
                currentSectionHeader = dateString;
                if( (today.get(Calendar.MONTH) < date.get(Calendar.MONTH)) || ((today.get(Calendar.DAY_OF_MONTH) < date.get(Calendar.DAY_OF_MONTH))) ){
                    dateString = "TOMORROW @ "+dateString;
                }
                games.add(dateString);
            }
            games.add(game);
        }


        Logger.d(TAG, "games size = " + gameArray.size());
        //for every game in the game array, give it an adapter and a item click listener to make bets on that game
        for(int i = 0; i < gameArray.size(); i++){
            Logger.d(TAG, gameArray.get(i).toString());
        }
        gamesListAdapter = new GamesListAdapter(activity.getApplicationContext(), games, application);

        gamesList.setAdapter(gamesListAdapter);
        gamesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object item = gamesListAdapter.getItem(position);
                if (item instanceof Game) {
                    // If clicked on game, go to bet activity
                    Log.d(TAG, "Clicked on game:" + item);

                    // Get game obj
                    Game game = (Game)item;
                    String gameId = game.getData_id();

                    Intent i = new Intent(activity, BetActivity.class);

                    // Send game id to the bet activity
                    i.putExtra("game_id", gameId);

                    // Start the activity
                    startActivity(i);
                }
            }
        });
    }

    @Override
    public void OnGetGamesSuccess() {
        mProgress.closeDialog();
        initGamesArray();
    }

    @Override
    public void OnGetGamesError(String message) {
        mProgress.closeDialog();
    }
}
