/**
 * Home Tab Fragment - class is the parent of Bet list, game list, and profile
 *
 * @desc - contains the "shortcuts" for datastore functions called often
 * @authors - Tim Mahler, Nick Burrin, Ryan Templin
 * @version - 1.0.0
 *
 */

package com.example.ryantemplin.payoutapp.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.example.ryantemplin.payoutapp.activities.HomeTabsActivity;
import com.example.ryantemplin.payoutapp.pojos.User;
import com.example.ryantemplin.payoutapp.util.DataStore;
import com.example.ryantemplin.payoutapp.util.MainApp;

/**
 * Created by ryantemplin on 5/9/15.
 */
public class HomeTabFragment extends Fragment {

    HomeTabsActivity activity;
    MainApp application;
    //User user;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (HomeTabsActivity) getActivity();
        application = (MainApp) activity.getApplication();
        //user = DataStore.getInstance().getUser();


    }
}
