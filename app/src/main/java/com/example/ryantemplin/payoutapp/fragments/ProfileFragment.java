/**
 * Profile Fragment - list of the bets, future, active, completed
 *
 * @desc - Fragment that contains user profile information
 * @authors - Tim Mahler, Nick Burrin, Ryan Templin
 * @version - 1.0.0
 */

package com.example.ryantemplin.payoutapp.fragments;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ryantemplin.payoutapp.R;
import com.example.ryantemplin.payoutapp.activities.AboutUsActivity;
import com.example.ryantemplin.payoutapp.activities.CashOutActivity;
import com.example.ryantemplin.payoutapp.activities.LoginActivity;
import com.example.ryantemplin.payoutapp.activities.RegisterUserActivity;
import com.example.ryantemplin.payoutapp.pojos.User;
import com.example.ryantemplin.payoutapp.util.DataStore;
import com.example.ryantemplin.payoutapp.util.MainApp;
import com.example.ryantemplin.payoutapp.util.MoneyUtil;

public class ProfileFragment extends HomeTabFragment {

    private static final String TAG = ProfileFragment.class.getSimpleName();

    private MainApp mApplication;
    private Button mButtonCashOut;
    private Button mButtonAboutUs;
    private Button mButtonSignOut;
    private final int MIN_ACCT_AMOUNT = 10000;

    private TextView mMyBalanceView;
    private Toast toast;
    private TextView mToastText;


    @Override
    public void onCreate(Bundle savedInstanceState) {super.onCreate(savedInstanceState);}

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initFragment();
    }

    @Override
    public void onResume() {
        super.onResume();
        mMyBalanceView.setText(MoneyUtil.intToDollar(DataStore.getInstance().getUser().getCredit()));
    }

 /*
 * Initialize the custom toast view for cashing out
 */
    private void initCustomToast(){
        LayoutInflater inflater = activity.getLayoutInflater();
        View layout = inflater.inflate(R.layout.view_toast_layout,
                (ViewGroup) activity.findViewById(R.id.toast_layout_root));

        mToastText = (TextView) layout.findViewById(R.id.text);

        // Set custom toast
        mToastText.setText("You cannot cash out with less than " + MoneyUtil.intToDollar(MIN_ACCT_AMOUNT));   // Set toast message

        toast = new Toast(activity.getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        mApplication = application;

        mButtonCashOut = (Button) view.findViewById(R.id.button_cash_out);
        mButtonAboutUs = (Button) view.findViewById(R.id.button_about_us);
        mButtonSignOut = (Button) view.findViewById(R.id.button_sign_out);

        //grab the text view for name in XML
        TextView mName= (TextView) view.findViewById(R.id.tv_my_name);
        //put the value from the User sign in page into XML

        String userFullName = DataStore.getInstance().getUser().getFirstname() + " " + DataStore.getInstance().getUser().getLastname();
        //custom toast for cashing
        initCustomToast();

        mName.setText(userFullName);
        mMyBalanceView = ((TextView) view.findViewById(R.id.tv_balance_amount));
        mMyBalanceView.setText(MoneyUtil.intToDollar(DataStore.getInstance().getUser().getCredit()));

        mButtonCashOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(DataStore.getInstance().getUser().getCredit() < MIN_ACCT_AMOUNT){
                    toast.show();
                } else {
                    Intent i = new Intent(activity, CashOutActivity.class);
                    startActivity(i);
                }
            }
        });
        //on click listener for the sign out button
        mButtonSignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setMessage("Are you sure you want to log out?").setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();
            }
        });
        //on click listener for the sign out button
        mButtonAboutUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(activity, AboutUsActivity.class);
                startActivity(i);
            }
        });

        return view;
    }

    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which){
                case DialogInterface.BUTTON_POSITIVE:
                    //Yes button clicked
                    mApplication.logOut();
                    Intent i = new Intent(activity, LoginActivity.class);
                    startActivity(i);
                    //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    activity.overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
                    break;

                case DialogInterface.BUTTON_NEGATIVE:
                    //No button clicked
                    break;
            }
        }
    };

    /**
     * Initializes all necessary fields, adds listeners, and starts the bidding engine.
     */
    public void initFragment() {

    }
}