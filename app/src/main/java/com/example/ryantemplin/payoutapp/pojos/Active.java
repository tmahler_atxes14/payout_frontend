/**
 * Active Status - extends status
 *
 * @authors - Tim Mahler, Nick Burrin, Ryan Templin
 * @version - 1.0.0
 */

package com.example.ryantemplin.payoutapp.pojos;

/**
 * Created by nickburrin on 4/29/15.
 */
public class Active extends Status {
    private String awayUrl;
    private String homeUrl;

    public Active(String away_url, String home_url){
        super(false);
        this.awayUrl = away_url;
        this.homeUrl = home_url;

    }

    public String getAwayUrl() {
        return awayUrl;
    }

    public void setAwayUrl(String awayUrl) {
        this.awayUrl = awayUrl;
    }

    public String getHomeUrl() {
        return homeUrl;
    }

    public void setHomeUrl(String homeUrl) {
        this.homeUrl = homeUrl;
    }

}
