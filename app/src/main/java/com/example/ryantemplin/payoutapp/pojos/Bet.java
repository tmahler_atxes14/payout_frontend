package com.example.ryantemplin.payoutapp.pojos;

import com.example.ryantemplin.payoutapp.util.DataStore;

import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;

/**
 * Bet Abstract class
 *
 * @desc - abstract class that contain amount bet, dataid, game id, amount bet, and odd for bet
 * @authors - Tim Mahler, Nick Burrin, Ryan Templin
 * @version - 1.0.0
 */
abstract public class Bet implements Comparable{
    public static final boolean WON = true;
    public static final boolean LOST = false;

    String game_id;
    int bet_id;
    String data_id;
    int betAmount;  //In Cents!!!!
	double odds;

    public Bet(String g, String data_id, int amnt, double odds){
        this.game_id = g;
        this.data_id = data_id;
        this.betAmount = amnt;
        this.odds = odds;
    }

	public Game getGame() {
		return DataStore.getInstance().getGameHash().get(game_id);
	}

	public String getGame_id() {
		return game_id;
	}

	public void setGame_id(String game) {
		this.game_id = game;
	}

    public int getResultAmount(boolean didWin){
        if(didWin == WON){
            return betAmount + betAmount;
        } else{
            return betAmount*-1;
        }
    }

	public double getOdds() {
		return odds;
	}

	public void setOdds(double odds) {
		this.odds = odds;
	}

	public int getAmount() {
		return betAmount;
	}

	public void setAmount(int amount) {
		this.betAmount = amount;
	}

    public int getBet_id() {
        return bet_id;
    }

    public void setBet_id(int bet_id) {
        this.bet_id = bet_id;
    }

    public String getData_id() {
        return data_id;
    }

    public void setData_id(String data_id) {
        this.data_id = data_id;
    }

    public int compareTo(Object o) {
        if (!(o instanceof Bet))
            throw new ClassCastException();

        Bet b = (Bet) o;

        return this.compareTo(b);
    }

    public Calendar getDate() {
        return getGame().getDate();
    }

    public static class BetComparator implements Comparator<Bet> {
        public int compare(Bet b1, Bet b2) {
            Calendar b1_date = b1.getDate();
            Calendar b2_date = b2.getDate();
            return b1_date.compareTo(b2_date);
        }
    }
}
