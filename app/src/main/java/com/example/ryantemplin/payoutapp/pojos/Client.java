package com.example.ryantemplin.payoutapp.pojos;

import java.util.ArrayList;


/**
 * Client
 *
 * @desc - information for the Client
 * @authors - Tim Mahler, Nick Burrin, Ryan Templin
 * @version - 1.0.0
 */

public class Client {
	String name;
	String email;
	String password;
	double AccountBalance;
	ArrayList<Bet> betHistory;
	
	public Client(String n, String e, String p){
		this.name = n;
		this.email = e;
		this.password = p;
		this.betHistory = new ArrayList<Bet>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public double getAccountBalance() {
		return AccountBalance;
	}

	public void setAccountBalance(double accountBalance) {
		AccountBalance = accountBalance;
	}

	public ArrayList<Bet> getBetHistory() {
		return betHistory;
	}

	public void setBetHistory(ArrayList<Bet> betHistory) {
		this.betHistory = betHistory;
	}
}
