package com.example.ryantemplin.payoutapp.pojos;

/**
 * Completed Status - extends status
 *
 * @authors - Tim Mahler, Nick Burrin, Ryan Templin
 * @version - 1.0.0
 */
public class Completed extends Status {
    private String awayUrl;
    private String homeUrl;
    private Integer gameWinnings;
    private Integer homeRuns;
    private Integer awayRuns;

    public Completed(String away_url, String home_url, Integer gameWinnings, Integer homeRuns, Integer awayRuns){
        super(false);
        this.awayUrl = away_url;
        this.homeUrl = home_url;
        this.gameWinnings = gameWinnings;
        this.homeRuns = homeRuns;
        this.awayRuns = awayRuns;
    }

    public String getAwayUrl() {
        return awayUrl;
    }

    public void setAwayUrl(String awayUrl) {
        this.awayUrl = awayUrl;
    }

    public String getHomeUrl() {
        return homeUrl;
    }

    public void setHomeUrl(String homeUrl) {
        this.homeUrl = homeUrl;
    }

    public Integer getGameWinnings() {
        return gameWinnings;
    }

    public void setGameWinnings(Integer gameWinnings) {
        this.gameWinnings = gameWinnings;
    }

    public Integer getHomeRuns() {
        return homeRuns;
    }

    public void setHomeRuns(Integer homeRuns) {
        this.homeRuns = homeRuns;
    }

    public Integer getAwayRuns() {
        return awayRuns;
    }

    public void setAwayRuns(Integer awayRuns) {
        this.awayRuns = awayRuns;
    }
}
