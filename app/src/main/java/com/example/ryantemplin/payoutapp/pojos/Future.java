package com.example.ryantemplin.payoutapp.pojos;

/**
 * Active Status - extends status
 *
 * @authors - Tim Mahler, Nick Burrin, Ryan Templin
 * @version - 1.0.0
 */
public class Future extends Status {
    public Future(){
        super(true);
    }
}
