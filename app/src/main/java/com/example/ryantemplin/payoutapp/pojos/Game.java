package com.example.ryantemplin.payoutapp.pojos;

import com.example.ryantemplin.payoutapp.util.DateUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Comparator;

/**
 * Game class
 *
 * @desc - details about game
 * @authors - Tim Mahler, Nick Burrin, Ryan Templin
 * @version - 1.0.0
 */
public class Game implements Comparable {
    private int game_id;
    private String data_id;
    private int game_over;
    private int date_unix;

    private String winner_id;
    private Team homeTeam;
    private Team awayTeam;

    double overUnderMultiplier;
    double winnerLineMultiplier;



    private LineOdds gameLineOdds;
    private OverUnderOdds gameOverUnderOdds;

    Calendar date;
    String date_string;




    public Game(JSONObject gameObj, Team homeTeam, Team awayTeam) {
        //game attributes
        try {
            this.game_id = gameObj.getInt("id");
            this.data_id = gameObj.getString("data_id");
            this.game_over = gameObj.getInt("game_over");
            this.winner_id = gameObj.getString("winner_id");

            //get odds JSON Objects for the game
            JSONObject winnerLineOddsObj = gameObj.getJSONObject("winner_line");
            JSONObject overUnderOddsObj = gameObj.getJSONObject("over_under");

            //Make odds objects
            gameLineOdds = new LineOdds(winnerLineOddsObj);
            gameOverUnderOdds = new OverUnderOdds(overUnderOddsObj);

            // Get date
            this.date_unix = gameObj.getInt("date");
            this.date = DateUtil.toCalendarFromUNIX(date_unix);
            this.date_string = DateUtil.calToString(this.date);


            this.homeTeam = homeTeam;
            this.awayTeam = awayTeam;

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public int compareTo(Object o) {
        if (!(o instanceof Game))
            throw new ClassCastException();

        Game e = (Game) o;

        return this.compareTo(e);
    }

    public static class GameComparator implements Comparator<Game> {
        public int compare(Game g1, Game g2) {
            Calendar g1_date = g1.getDate();
            Calendar g2_date = g2.getDate();
            System.out.println("GAMES :"+g1+", #2:"+g2);
            return g1_date.compareTo(g2_date);
        }
    }


    @Override
    public String toString(){

        return "Game:"+game_id+", data_id:"+data_id+", date:"+ date_unix;
    }

    // Getters and setters
    public String getData_id() {
        return data_id;
    }

    public void setData_id(String data_id) {
        this.data_id = data_id;
    }

    public int getGame_over() {
        return game_over;
    }

    public void setGame_over(int game_over) {
        this.game_over = game_over;
    }

    public Team getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(Team homeTeam) {
        this.homeTeam = homeTeam;
    }

    public String getWinner_id() {
        return winner_id;
    }

    public void setWinner_id(String winner_id) {
        this.winner_id = winner_id;
    }

    public Team getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(Team awayTeam) {
        this.awayTeam = awayTeam;
    }

    public int getGame_id() {

        return game_id;
    }

    public void setGame_id(int game_id) {
        this.game_id = game_id;
    }

    public int getDate_unix() {
        return date_unix;
    }

    public void setDate_unix(int date_unix) {
        this.date_unix = date_unix;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public String getDate_string() {
        return date_string;
    }

    public void setDate_string(String date_string) {
        this.date_string = date_string;
    }

    public OverUnderOdds getGameOverUnderOdds() {
        return gameOverUnderOdds;
    }

    public void setGameOverUnderOdds(OverUnderOdds gameOverUnderOdds) {
        this.gameOverUnderOdds = gameOverUnderOdds;
    }

    public LineOdds getGameLineOdds() {
        return gameLineOdds;
    }

    public void setGameLineOdds(LineOdds gameLineOdds) {
        this.gameLineOdds = gameLineOdds;
    }

    public double getWinnerLineMultiplier() {
        return winnerLineMultiplier;
    }

    public void setWinnerLineMultiplier(double winnerLineMultiplier) {
        this.winnerLineMultiplier = winnerLineMultiplier;
    }

    public double getOverUnderMultiplier() {
        return overUnderMultiplier;
    }

    public void setOverUnderMultiplier(double overUnderMultiplier) {
        this.overUnderMultiplier = overUnderMultiplier;
    }

}
