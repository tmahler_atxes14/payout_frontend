package com.example.ryantemplin.payoutapp.pojos;

/**
 * GameBet class
 *
 * @desc - wrapper class for all the bets for a given game
 * @authors - Tim Mahler, Nick Burrin, Ryan Templin
 * @version - 1.0.0
 */
public class GameBet {
    private String game_id;
    private Status status;
    private int betResult;
    private LineWinnerBet lwb;
    private OverUnderBet oub;
    private SuperStarBet awaySuperStarBet;
    private SuperStarBet homeSuperStarBet;
    private Integer totalAmountSpent;

    public GameBet(String game_id, Status status, LineWinnerBet linebet, OverUnderBet oubet,
                   SuperStarBet awaySSB, SuperStarBet homeSSB, Integer totalAmountSpent){

        this.game_id = game_id;
        this.status = status;
        this.lwb = linebet;
        this.oub = oubet;
        this.awaySuperStarBet = awaySSB;
        this.homeSuperStarBet = homeSSB;
        this.totalAmountSpent = totalAmountSpent;

    }

    @Override
    public String toString() {
        return "Game Bet: line="+this.lwb+", over/under="+this.oub+", awayss="+this.awaySuperStarBet+", homess="+this.homeSuperStarBet;
    }

    public String getGameID() {
        return game_id;
    }

    public void setGameID(String game_id) {
        this.game_id = game_id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public int getBetResultAmount(boolean didWin) {
        return lwb.getResultAmount(didWin) + oub.getResultAmount(didWin) +
                awaySuperStarBet.getResultAmount(didWin) + homeSuperStarBet.getResultAmount(didWin);
    }

    public OverUnderBet getOub() {
        return oub;
    }

    public String getGame_id() {
        return game_id;
    }

    public int getBetResult() {
        return betResult;
    }

    public LineWinnerBet getLwb() {
        return lwb;
    }

    public SuperStarBet getAwaySuperStarBet() {
        return awaySuperStarBet;
    }

    public SuperStarBet getHomeSuperStarBet() {
        return homeSuperStarBet;
    }

    public Integer getTotalAmountSpent() {
        return totalAmountSpent;
    }

    public void setTotalAmountSpent(Integer totalAmountSpent) {
        this.totalAmountSpent = totalAmountSpent;
    }
}
