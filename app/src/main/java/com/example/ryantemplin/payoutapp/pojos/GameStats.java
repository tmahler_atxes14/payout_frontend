package com.example.ryantemplin.payoutapp.pojos;
/**
 * GameStats class
 *
 * @desc - contains stats for each game
 * @authors - Tim Mahler, Nick Burrin, Ryan Templin
 * @version - 1.0.0
 */
public class GameStats {
	int id;
	String teamId;
	String gameId;
	int num_runs;
	int num_hits;
	
	public int getRuns() {
		return num_runs;
	}
}
