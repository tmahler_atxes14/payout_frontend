package com.example.ryantemplin.payoutapp.pojos;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * LineOdds class
 *
 * @desc - contains all odds attributes that are exclusive to a linebet
 * @authors - Tim Mahler, Nick Burrin, Ryan Templin
 * @version - 1.0.0
 */
public class LineOdds extends Odds{
    private int lineId;
    private String teamId;
    private double line;
    private double oddsMultiplier;

    public LineOdds (JSONObject LineOddsObj){
        try {
            lineId = LineOddsObj.getInt("id");
            teamId = LineOddsObj.getString("team_id");
            line = LineOddsObj.getDouble("line");
            oddsMultiplier = LineOddsObj.getDouble("odds_multiplier");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public double getOddsMultiplier() {
        return oddsMultiplier;
    }

    public void setOddsMultiplier(double oddsMultiplier) {
        this.oddsMultiplier = oddsMultiplier;
    }

    public double getLine() {
        return line;
    }

    public void setLine(double line) {
        this.line = line;
    }

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public int getLineId() {
        return lineId;
    }

    public void setLineId(int lineId) {
        this.lineId = lineId;
    }

}
