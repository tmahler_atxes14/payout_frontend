package com.example.ryantemplin.payoutapp.pojos;

/**
 * LineWinnerBet class
 *
 * @desc - contains all attributes that are exclusive to a linebet
 * @authors - Tim Mahler, Nick Burrin, Ryan Templin
 * @version - 1.0.0
 */
public class LineWinnerBet extends Bet{
	private String choiceTeamId;
    public LineWinnerBet(String g, String data_id, int amnt, String teamId) {
		super(g, data_id, amnt, 1);
		choiceTeamId = teamId;
	}

    public String getChoiceTeamId() {
        return choiceTeamId;
    }

    public void setChoiceTeamId(String choiceTeamId) {
        this.choiceTeamId = choiceTeamId;
    }



}
