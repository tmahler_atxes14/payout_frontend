package com.example.ryantemplin.payoutapp.pojos;

/**
 * OverUnderBet class
 *
 * @desc - contains all attributes that are exclusive to a over/under bet
 * @authors - Tim Mahler, Nick Burrin, Ryan Templin
 * @version - 1.0.0
 */
public class OverUnderBet extends Bet{
	public final static int UNDER = 0;
	public final static int OVER = 1;
	
	int overUnderScore; 
	int overUnder;

    public OverUnderBet(String g, String data_id, int amnt, int choice) {
        super(g, data_id, amnt, 1);
        this.overUnder = choice;
    }
	/*
	public OverUnderBet(Game g, double betAmount, int score, int choice) {
		super(g, betAmount);
		this.overUnderScore = score;
		this.overUnder = choice;
	}
	*/

	public int getOverUnderScore() {
		return overUnderScore;
	}

	public void setOverUnderScore(int overUnderScore) {
		this.overUnderScore = overUnderScore;
	}

	public int getOverOrUnder() {
		return overUnder;
	}

	public void setOverOrUnder(int overOrUnder) {
		this.overUnder = overOrUnder;
	}
}
