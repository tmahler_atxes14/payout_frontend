package com.example.ryantemplin.payoutapp.pojos;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * OverUnderOdds class
 *
 * @desc - contains all odds attributes that are exclusive to a over/under bet
 * @authors - Tim Mahler, Nick Burrin, Ryan Templin
 * @version - 1.0.0
 */
public class OverUnderOdds extends Odds{


    private int lineId;
    private double overUnderValue;
    private double oddsMultiplier;

    public OverUnderOdds (JSONObject OverUnderOddsObj){
        try {
            lineId = OverUnderOddsObj.getInt("id");
            overUnderValue = OverUnderOddsObj.getDouble("over_under_value");
            oddsMultiplier = OverUnderOddsObj.getDouble("odds_multiplier");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public int getLineId() {
        return lineId;
    }

    public void setLineId(int lineId) {
        this.lineId = lineId;
    }

    public double getOddsMultiplier() {
        return oddsMultiplier;
    }

    public void setOddsMultiplier(double oddsMultiplier) {
        this.oddsMultiplier = oddsMultiplier;
    }

    public double getOverUnderValue() {
        return overUnderValue;
    }

    public void setOverUnderValue(double overUnderValue) {
        this.overUnderValue = overUnderValue;
    }
}