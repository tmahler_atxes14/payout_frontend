package com.example.ryantemplin.payoutapp.pojos;

/**
 * Status class
 *
 * @desc - contains whether the status is editable or not
 * @authors - Tim Mahler, Nick Burrin, Ryan Templin
 * @version - 1.0.0
 */
public class Status {
    private boolean editable;

    public Status(boolean editable){
        this.editable = editable;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }
}
