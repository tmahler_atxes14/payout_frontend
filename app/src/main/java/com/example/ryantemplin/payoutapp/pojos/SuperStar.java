package com.example.ryantemplin.payoutapp.pojos;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * SuperStar object
 *
 * @authors - Tim Mahler, Nick Burrin, Ryan Templin
 * @version - 1.0.0
 */
public class SuperStar {

    private int id;
    private String teamId;
    private String playerId;
	private String name;
    private int numHomeRuns;
    private int numGamesPlayed;

    private SuperStarOdds mySuperStarOdds;
	
	public SuperStar(JSONObject SuperStarObj) {
		super();
        try{
            id = SuperStarObj.getInt("id");
            playerId = SuperStarObj.getString("data_id");
            teamId = SuperStarObj.getString("team_id");
            name = SuperStarObj.getString("first_name") + " " + SuperStarObj.getString("last_name");
            numHomeRuns = SuperStarObj.getInt("num_total_homeruns");
            numGamesPlayed = SuperStarObj.getInt("num_games_played");
            //get superstar odds obj, and then make a superstar odds constructor
            JSONObject superStarOddsObj = SuperStarObj.getJSONObject("odds");
            mySuperStarOdds = new SuperStarOdds(superStarOddsObj);

        } catch (JSONException e) {
            e.printStackTrace();
        }
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

    public SuperStarOdds getMySuperStarOdds() {
        return mySuperStarOdds;
    }

    public void setMySuperStarOdds(SuperStarOdds mySuperStarOdds) {
        this.mySuperStarOdds = mySuperStarOdds;
    }

    public int getNumHomeRuns() {
        return numHomeRuns;
    }

    public void setNumHomeRuns(int numHomeRuns) {
        this.numHomeRuns = numHomeRuns;
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public int getNumGamesPlayed() {
        return numGamesPlayed;
    }

    public void setNumGamesPlayed(int numGamesPlayed) {
        this.numGamesPlayed = numGamesPlayed;
    }

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

//	public String getTeam() 	return team;	}

//	public void setTeam(String team) {		this.team = team;	}
}
