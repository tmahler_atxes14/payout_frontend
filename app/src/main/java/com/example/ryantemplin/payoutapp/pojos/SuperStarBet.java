package com.example.ryantemplin.payoutapp.pojos;


/**
 * SuperStarBet class
 *
 * @desc - contains all attributes that are exclusive to a superstar bet
 * @authors - Tim Mahler, Nick Burrin, Ryan Templin
 * @version - 1.0.0
 */
public class SuperStarBet extends Bet{
    private int HR;
    private String playerId;

	public SuperStarBet(String g, String data_id, int betAmount, double odds, String playerId, int yes) {
		super(g, data_id, betAmount, odds);
		// TODO add a variable that will hold the user's bet
        HR = yes;
        this.playerId = playerId;
	}

    @Override
    public int getResultAmount(boolean didWin){
        if(didWin == WON){
            return betAmount + (int)(betAmount*odds);
        } else{
            return betAmount*-1;
        }
    }

    public int getHR() {
        return HR;
    }


    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }


}
