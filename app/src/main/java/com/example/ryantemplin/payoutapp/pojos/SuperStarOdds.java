package com.example.ryantemplin.payoutapp.pojos;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * SuperStarOdds class
 *
 * @desc - contains all odds attributes that are exclusive to a superstar bet
 * @authors - Tim Mahler, Nick Burrin, Ryan Templin
 * @version - 1.0.0
 */
public class SuperStarOdds extends Odds {
    private int oddsId;
    private String superStarId;
    private String gameId;
    private double oddsMultiplierYesHR;

    public SuperStarOdds (JSONObject SuperStarOddsObj){
        try {
            oddsId = SuperStarOddsObj.getInt("id");
            superStarId = SuperStarOddsObj.getString("superstar_id");
            gameId = SuperStarOddsObj.getString("game_id");
            oddsMultiplierYesHR = SuperStarOddsObj.getDouble("odds_multiplier");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public double getOddsMultiplierYesHR() {
        return oddsMultiplierYesHR;
    }

    public void setOddsMultiplierYesHR(double oddsMultiplierYesHR) {
        this.oddsMultiplierYesHR = oddsMultiplierYesHR;
    }

    public int getOddsId() {
        return oddsId;
    }

    public void setOddsId(int oddsId) {
        this.oddsId = oddsId;
    }

    public String getSuperStarId() {
        return superStarId;
    }

    public void setSuperStarId(String superStarId) {
        this.superStarId = superStarId;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

}