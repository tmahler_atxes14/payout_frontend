package com.example.ryantemplin.payoutapp.pojos;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Team class
 *
 * @desc - contains all attributes that for a team
 * @authors - Tim Mahler, Nick Burrin, Ryan Templin
 * @version - 1.0.0
 */
public class Team {

	// Custom attributes
	private int gameId;
	private String teamId;
	private String city;
	private String teamName;
	private String abbr;
	private String logoUrl;
	private SuperStar superStar;
    private JSONObject superStarObj;
	private String record;
	private int wins;
	private int losses;

	// Create Team object from JSON object
    public Team(JSONObject TeamsObj) {
        try {
            gameId = TeamsObj.getInt("id");
            teamId = TeamsObj.getString("data_id");
            teamName = TeamsObj.getString("name");
            city = TeamsObj.getString("market");
            abbr = TeamsObj.getString("abbr");
            logoUrl = TeamsObj.getString("logo_url");
            wins = TeamsObj.getInt("num_total_wins");
            losses = TeamsObj.getInt("num_total_losses");
            record = wins + " - " + losses;
            superStarObj = TeamsObj.getJSONObject("superstar");

            //create the superstar objects
            // if superstar returns null, need to be able to handle that
            superStar = new SuperStar(superStarObj);


            // find the superstar object
            //this.superStar = teamSuperStar;

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

	@Override
	public String toString() {
		return "Team Id: "+this.teamId+" --> "+this.city+" "+this.teamName;
	}

	// Getters and setters
    public String getRecord() {
        return record;
    }

    public void setRecord(String record) {
        this.record = record;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

	public int getGameId() {
		return gameId;
	}

	public void setGameId(int id) {
		this.gameId = id;
	}

	public String getTeamId() {
		return teamId;
	}

	public void setTeamId(String dataId) {
		this.teamId = dataId;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public String getAbbr() {
		return abbr;
	}

	public void setAbbr(String abbr) {
		this.abbr = abbr;
	}

	public SuperStar getSuperStar() {
		return superStar;
	}

	public void setSuperStar(SuperStar superStar) {
		this.superStar = superStar;
	}

	public int getWins() {
		return wins;
	}

	public void setWins(int wins) {
		this.wins = wins;
	}

	public int getLosses() {
		return losses;
	}

	public void setLosses(int losses) {
		this.losses = losses;
	}
}
