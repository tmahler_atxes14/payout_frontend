package com.example.ryantemplin.payoutapp.util;

import com.example.ryantemplin.payoutapp.pojos.Bet;
import com.example.ryantemplin.payoutapp.pojos.Game;
import com.example.ryantemplin.payoutapp.pojos.GameBet;
import com.example.ryantemplin.payoutapp.pojos.Team;
import com.example.ryantemplin.payoutapp.pojos.User;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by tim mahler & ryan templin on 4/7/15.
 */
public class DataStore {
    private static final String TAG = DataStore.class.getSimpleName();
    private static DataStore instance = null;
    private ArrayList<Game> gameArray;
    private HashMap<String, Game> gameHash; // HashMap of Game object (key == id)
    private HashMap<String, Team> teamsHash;

    // Data objects
    private User user;

    private DataStore(){
        gameArray = new ArrayList<Game>();
        gameHash = new HashMap<String, Game>();
        teamsHash = new HashMap<String, Team>();
    }

    public static DataStore getInstance(){
        if(instance == null){
            synchronized(DataStore.class){
                if(instance == null){
                    instance = new DataStore();
                }
            }
        }
        return instance;
    }

    // Getters and setters
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ArrayList<Game> getGameArray() {
        return gameArray;
    }

    public void setGameArray(ArrayList<Game> gameArray) {
        this.gameArray = gameArray;
    }

    public HashMap<String, Game> getGameHash() {
        return gameHash;
    }

    public void setGameHash(HashMap<String, Game> gameHash) {
        this.gameHash = gameHash;
    }


    public HashMap<String, Team> getTeamsHash() {
        return teamsHash;
    }

    public void setTeamsHash(HashMap<String, Team> teamsHash) {
        this.teamsHash = teamsHash;
    }
}
