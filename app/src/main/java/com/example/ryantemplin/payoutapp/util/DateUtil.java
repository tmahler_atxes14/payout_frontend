package com.example.ryantemplin.payoutapp.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * Created by tim-azul on 4/16/15.
 */
public class DateUtil {

    /** Transform ISO 8601 string to Calendar. */
    public static Calendar toCalendar(final String iso8601string)
            throws ParseException {
        Calendar calendar = GregorianCalendar.getInstance();
        String s = iso8601string.replace("Z", "+00:00");
        try {
            s = s.substring(0, 22) + s.substring(23);  // to get rid of the ":"
        } catch (IndexOutOfBoundsException e) {
            throw new ParseException("Invalid length", 0);
        }
        Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").parse(s);
        calendar.setTime(date);
        return calendar;
    }

    public static Calendar toCalendarFromUNIX(final int unixTimeStamp){
        Date date = new Date ();
        date.setTime((long)unixTimeStamp*1000);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }

    public static Calendar convertFromGmt(Calendar cal) {

        Calendar c =  Calendar.getInstance();
        Date date= new Date();
        TimeZone tz = TimeZone.getDefault();
        cal.setTimeZone(TimeZone.getTimeZone("GMT"));

        //Returns the number of milliseconds since January 1, 1970, 00:00:00 GMT
        long msFromEpochGmt = date.getTime();

        //gives you the current offset in ms from GMT at the current date
        int offsetFromUTC = tz.getOffset(msFromEpochGmt);
        System.out.println("OFFSEt=" + tz);

        //create a new calendar in GMT timezone, set to this date and add the offset
        cal.add(Calendar.MILLISECOND, offsetFromUTC);

        return cal;
    }

    public static Calendar convertToGmt(Calendar cal) {

        Calendar c =  Calendar.getInstance();
        Date date= new Date();
        TimeZone tz = TimeZone.getDefault();
        cal.setTimeZone(TimeZone.getTimeZone("GMT"));

        //Returns the number of milliseconds since January 1, 1970, 00:00:00 GMT
        long msFromEpochGmt = date.getTime();

        //gives you the current offset in ms from GMT at the current date
        int offsetFromUTC = 0 - tz.getOffset(msFromEpochGmt);
        System.out.println("OFFSEt="+tz);

        //create a new calendar in GMT timezone, set to this date and add the offset
        cal.add(Calendar.MILLISECOND, offsetFromUTC);

        return cal;
    }

    public static String calToString(final Calendar cal){
        DateFormat df = new SimpleDateFormat("h:mm a");

        // Get the date today using Calendar object.
        Date today = cal.getTime();
        // Using DateFormat format method we can create a string
        // representation of a date with the defined format.
        return df.format(today);
    }

    public static String calToTimeString(final Calendar cal){
        DateFormat df = new SimpleDateFormat("h:mm a");

        // Get the date today using Calendar object.
        Date today = cal.getTime();
        // Using DateFormat format method we can create a string
        // representation of a date with the defined format.
        return df.format(today);
    }
}
