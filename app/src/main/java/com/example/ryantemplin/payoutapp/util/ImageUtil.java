package com.example.ryantemplin.payoutapp.util;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.utils.StorageUtils;

import java.io.File;
import java.lang.ref.WeakReference;

/**
 * Created by brian on 7/25/14.
 */
public class ImageUtil {

    private static ImageUtil instance = null;
    private Context mContext;
    private Bitmap defaultFavoriteIcon = null;
    private Bitmap defaultPaymentIcon = null;
    private Bitmap defaultPurchasesIcon = null;

    public ImageUtil(Context context){
        mContext = context;
    }

    /**
     * Returns singleton instance | creates new instance
     *
     * @return DataStore instance
     */
    public static ImageUtil getInstance(Context context) {
        if (instance == null) {
            instance = new ImageUtil(context);
        }
        return instance;
    }

    public static void displayHighResImage(ImageView iview, String url) {
        if (iview == null)
            return;

        BitmapFactory.Options bmpOptions = new BitmapFactory.Options();
        bmpOptions.inPurgeable = true;
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .decodingOptions(bmpOptions)
                .cacheInMemory(false).build();

        displayImage(iview, url, url, options);

    }

    public static void displayImage(final ImageView iview, final String finalURL, String initialURL,
                                    DisplayImageOptions options) {
        if (initialURL == null || initialURL.isEmpty())
            initialURL = finalURL;

        final boolean urlsEqual = finalURL != null && finalURL.equals(initialURL);

        if (options == null) {
            ImageLoader.getInstance().displayImage(initialURL, iview, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String s, View view) {

                }

                @Override
                public void onLoadingFailed(String s, View view, FailReason failReason) {
                    if (!urlsEqual)
                        ImageLoader.getInstance().displayImage(finalURL, iview);
                }

                @Override
                public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                    if (!urlsEqual && finalURL != null && !finalURL.isEmpty())
                        ImageLoader.getInstance().displayImage(finalURL, iview);
                }

                @Override
                public void onLoadingCancelled(String s, View view) {

                }
            });
        } else {
            ImageLoader.getInstance().displayImage(initialURL, iview, options, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String s, View view) {

                }

                @Override
                public void onLoadingFailed(String s, View view, FailReason failReason) {
                    ImageLoader.getInstance().displayImage(finalURL, iview);
                }

                @Override
                public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                    if (finalURL != null && !finalURL.isEmpty())
                        ImageLoader.getInstance().displayImage(finalURL, iview);
                }

                @Override
                public void onLoadingCancelled(String s, View view) {

                }
            });
        }
    }

    public static void initLoader(Context context) {
        BitmapFactory.Options bmpOptions = new BitmapFactory.Options();
        bmpOptions.inPreferredConfig = Bitmap.Config.RGB_565;
        bmpOptions.inPurgeable = true;
        bmpOptions.inInputShareable = true;
        File cacheDir = StorageUtils.getOwnCacheDirectory(context, "azul_cache");
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .cacheInMemory(false)
                .cacheOnDisk(true)
                .decodingOptions(bmpOptions)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .memoryCache(new WeakMemoryCache())
                .diskCache(new UnlimitedDiscCache(cacheDir))
                        // these lines contradict the line above
//                .diskCacheSize(20 * 1024 * 1024) // 20 MB
//                .diskCacheFileNameGenerator(new HashCodeFileNameGenerator())
                .denyCacheImageMultipleSizesInMemory()
                .defaultDisplayImageOptions(options)
                .build();
        ImageLoader.getInstance().init(config);
    }

    public static Bitmap decodeFromResource(Context context, int resource, BitmapFactory.Options options) {
        return BitmapFactory.decodeResource(context.getResources(), resource, options);
    }
}
