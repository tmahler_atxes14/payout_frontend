/**
 * Internet class handles POST and GET operations
 *
 * @author Patrick Monaghan & Tim Mahler
 * @version 1.0     1/26/2014
 */

package com.example.ryantemplin.payoutapp.util;

import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

public class Internet {

    private static final String TAG = Internet.class.getSimpleName();
    public static String INTERNET_FAILED = "Failed";
    public static String INTERNET_FAILED_MESSAGE = "Oops something went wrong!\nPlease try again later.";

    /**
     * PUT method
     *
     * @param url       - the url putting to
     * @param obj       - the JSON obj being put
     * @param authToken - the authToken
     * @return the JSON string object return from get request
     */
    public static String putToUrl(String url, JSONObject obj, String authToken) {
        try {
            //Log.d(TAG, "PUTTING --> " + obj + " at URL --> " + url);

            HttpParams httpParameters = new BasicHttpParams();
            // joliver: faster than default HTTP_1.0, not sure if this is still a problem, but shouldn't hurt
            HttpProtocolParams.setVersion(httpParameters, HttpVersion.HTTP_1_1);
            HttpConnectionParams.setConnectionTimeout(httpParameters, 3000); // 3s max for connection
            HttpConnectionParams.setSoTimeout(httpParameters, 4000); // 4s max to get data
            // TODO: use HTTPUrlConnection instead
            HttpClient httpclient = new DefaultHttpClient(httpParameters);
            HttpPut httpPut = new HttpPut(url.toString());
            httpPut.setHeader("Content-type", "application/json");
            if (authToken != null) {
                httpPut.setHeader("Authorization", "Basic " + authToken);
            }
            if (obj != null) {
                httpPut.setEntity(new ByteArrayEntity(
                        obj.toString().getBytes("UTF8")));
            }
            HttpResponse response = httpclient.execute(httpPut);
            HttpEntity entity = response.getEntity();
            InputStream is = entity.getContent(); // Create an InputStream with the response
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) // Read line by line
                sb.append(line + "\n");
            is.close();
            //Log.d(TAG, "PUT Response -> " + new JSONObject(sb.toString()));
            return sb.toString(); // Result is here*/
        } catch (Exception e) {
            //Log.e(TAG, e.getLocalizedMessage());
            return INTERNET_FAILED;
        }
    }

    /**
     * GET method
     *
     * @param url - the url getting from
     * @return the JSON string object return from get request
     */
    public static String getFromURL(String url, String authToken) {
        try {
            //Log.d(TAG, "GETTING from URL --> " + url);
            HttpParams httpParameters = new BasicHttpParams();
            // joliver: faster than defualt HTTP_1.0, not sure if this is still a problem, but shouldn't hurt
            HttpProtocolParams.setVersion(httpParameters, HttpVersion.HTTP_1_1);
            HttpConnectionParams.setConnectionTimeout(httpParameters, 3000); // 3s max for connection
            HttpConnectionParams.setSoTimeout(httpParameters, 4000); // 4s max to get data
            // TODO: use HTTPUrlConnection instead
            HttpClient httpclient = new DefaultHttpClient(httpParameters);
            HttpGet httpget = new HttpGet(url); // Set the action you want to do
            httpget.setHeader("Content-type", "application/json");
            httpget.setHeader("Authorization", "Basic " + authToken);
            HttpResponse response;
            response = httpclient.execute(httpget);
            HttpEntity entity = response.getEntity();
            InputStream is = entity.getContent(); // Create an InputStream with the response
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) // Read line by line
                sb.append(line + "\n");
            is.close();
            return sb.toString(); // Result is here*/
        } catch (Exception e) {
            e.printStackTrace();
            return INTERNET_FAILED;
        }
    }

    /**
     * GET method
     *
     * @param url - the url getting from
     * @return the JSON string object return from get request
     */
    public static String getFromURLWithoutAuth(String url) {
        try {
            Log.d(TAG, "GETTING from URL --> " + url);
            HttpParams httpParameters = new BasicHttpParams();
            // joliver: faster than defualt HTTP_1.0, not sure if this is still a problem, but shouldn't hurt
            HttpProtocolParams.setVersion(httpParameters, HttpVersion.HTTP_1_1);
            HttpConnectionParams.setConnectionTimeout(httpParameters, 3000); // 3s max for connection
            HttpConnectionParams.setSoTimeout(httpParameters, 4000); // 4s max to get data
            // TODO: use HTTPUrlConnection instead
            HttpClient httpclient = new DefaultHttpClient(httpParameters);
            HttpGet httpget = new HttpGet(url); // Set the action you want to do
            httpget.setHeader("Content-type", "application/json");
            HttpResponse response;
            response = httpclient.execute(httpget);
            HttpEntity entity = response.getEntity();
            InputStream is = entity.getContent(); // Create an InputStream with the response
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) // Read line by line
                sb.append(line + "\n");
            is.close();
            return sb.toString(); // Result is here*/
        } catch (Exception e) {
            e.printStackTrace();
            return INTERNET_FAILED;
        }
    }

    public static int getResponseCodeFromURL(String url, String authToken) {
        try {
            //Log.d(TAG, "GETTING from URL --> " + url);
            HttpParams httpParameters = new BasicHttpParams();
            // joliver: faster than defualt HTTP_1.0, not sure if this is still a problem, but shouldn't hurt
            HttpProtocolParams.setVersion(httpParameters, HttpVersion.HTTP_1_1);
            HttpConnectionParams.setConnectionTimeout(httpParameters, 3000); // 3s max for connection
            HttpConnectionParams.setSoTimeout(httpParameters, 4000); // 4s max to get data
            // TODO: use HTTPUrlConnection instead
            HttpClient httpclient = new DefaultHttpClient(httpParameters);
            HttpGet httpget = new HttpGet(url); // Set the action you want to do
            httpget.setHeader("Content-type", "application/json");
            httpget.setHeader("Authorization", "Basic " + authToken);
            HttpResponse response = httpclient.execute(httpget);
            return response.getStatusLine().getStatusCode();
        } catch (Exception e) {
            e.printStackTrace();
            return HttpStatus.SC_EXPECTATION_FAILED;
        }
    }

    /**
     * Deprecated, use {@link #post(String, JSONObject) post(String, JSONObject)} instead, it's faster.
     *
     * @param url     - the url posting to
     * @param jsonObj - the object that is being posted
     * @return the JSON string object return from post request
     */
    @Deprecated
    public static String postToURl(String url, JSONObject jsonObj) {
        try {
            Log.d(TAG, "POSTING --> " + jsonObj + " at URL --> " + url);

            HttpParams httpParameters = new BasicHttpParams();
            // joliver: faster than defualt HTTP_1.0, not sure if this is still a problem, but shouldn't hurt
            HttpProtocolParams.setVersion(httpParameters, HttpVersion.HTTP_1_1);
            HttpConnectionParams.setConnectionTimeout(httpParameters, 3000); // 3s max for connection
            HttpConnectionParams.setSoTimeout(httpParameters, 4000); // 4s max to get data
            HttpClient httpclient = new DefaultHttpClient(httpParameters);
            HttpPost httppost = new HttpPost(url); // Set the action you want to do
            httppost.setHeader("Content-type", "application/json");
            Log.d(TAG, "a");
            if (jsonObj != null) {
                httppost.setEntity(new ByteArrayEntity(
                        jsonObj.toString().getBytes("UTF8")));
            }
            Log.d(TAG, "b");
            HttpResponse response;
            response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            InputStream is = entity.getContent(); // Create an InputStream with the response
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) // Read line by line
                sb.append(line + "\n");
            is.close();
            Log.d(TAG, "POST Response -> " + new JSONObject(sb.toString()));
            return sb.toString(); // Result is here*/
        } catch (Exception e) {
            Log.e(TAG, e.getLocalizedMessage());
            return INTERNET_FAILED;
        }
    }


    public static String postToBitly(String url) {
        try {
            //Log.d(TAG, "POSTING --> " + jsonObj + " at URL --> " + url);

            HttpParams httpParameters = new BasicHttpParams();
            // joliver: faster than defualt HTTP_1.0, not sure if this is still a problem, but shouldn't hurt
            HttpProtocolParams.setVersion(httpParameters, HttpVersion.HTTP_1_1);
            HttpConnectionParams.setConnectionTimeout(httpParameters, 3000); // 3s max for connection
            HttpConnectionParams.setSoTimeout(httpParameters, 4000); // 4s max to get data


            HttpClient httpclient = new DefaultHttpClient(httpParameters);
            HttpPost httppost = new HttpPost("http://api.bit.ly/v3/shorten"); // Set the action you want to do

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(4);
            nameValuePairs.add(new BasicNameValuePair("apikey", "R_a7d73999c3bf48479acda540f5549c98"));
            nameValuePairs.add(new BasicNameValuePair("login", "tastebudbyazul"));
            nameValuePairs.add(new BasicNameValuePair("longurl", url));
            nameValuePairs.add(new BasicNameValuePair("format", "txt"));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            HttpResponse response;
            response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            InputStream is = entity.getContent(); // Create an InputStream with the response
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) // Read line by line
                sb.append(line + "\n");
            is.close();
            //Log.d(TAG, "POST Response -> " + new JSONObject(sb.toString()));
            return sb.toString(); // Result is here*/
        } catch (Exception e) {
            //Log.e(TAG, e.getLocalizedMessage());
            return INTERNET_FAILED;
        }
    }



    /**
     * POST using HttpsURLConnection
     *
     * @param urlString - the url posting to
     * @param jsonObj   - the object that is being posted
     * @return the JSON string object return from post request
     */
    public static String post(String urlString, JSONObject jsonObj) {

        try {
            URL url = new URL(urlString);
            return post(url, jsonObj, null);

        } catch (MalformedURLException e) {
            //Log.e(TAG, "malformed url: " + e.getLocalizedMessage());
            return "{\"error\" : \"malformed url\"}";
        }

    }

    /**
     * POST using HttpsURLConnection
     *
     * @param url       - the url posting to
     * @param jsonObj   - the object that is being posted
     * @param authToken
     * @return the JSON string object return from post request
     */
    public static String post(URL url, JSONObject jsonObj, String authToken) {
        try {
            //Log.d(TAG, "POSTING --> " + jsonObj + " at URL --> " + url.toString());

            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(3000);
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setUseCaches(false);

            // set request headers
            conn.setRequestProperty("Content-type", "application/json");
            if (authToken != null)
                conn.setRequestProperty("Authorization", "Basic " + authToken);

            // write request
            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            writer.write(jsonObj.toString());
            writer.flush();
            writer.close();
            os.close();

            int responseCode = conn.getResponseCode();
            //Log.d(TAG, "Sending 'POST' request to URL : " + url);
            //Log.d(TAG, "Response Code : " + responseCode);

            // read response
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            //Log.d(TAG, "POST Response -> " + response.toString());
            return response.toString();

        } catch (IOException e) {
            return INTERNET_FAILED;
        }
    }

    public static String delete(String urlString, String authToken, String[] params, Object... values) {
        try {
//            if (params.length != values.length)
//                throw new InputMismatchException("params.length != values.length");

            if (!urlString.endsWith("/"))
                urlString += "/";
            urlString += "?";

//            for (int k = 0; k < params.length; k++)
//                urlString += params[k] + "=" + values[k] + "&";

            //urlString = urlString.substring(0, urlString.length() - 1);

            Log.d(TAG, "DELETE --> " + urlString);

            URL url = new URL(urlString);

            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(3000);
            conn.setRequestMethod("DELETE");
            conn.setUseCaches(false);

            // set request headers
//            if (authToken != null)
//                conn.setRequestProperty("Authorization", "Basic " + authToken);

            int responseCode = conn.getResponseCode();
            Log.d(TAG, "Sending 'DELETE' request to URL : " + url);
            Log.d(TAG," responseCode "+responseCode);
            if (responseCode == 500) return "Error: card has been deleted too many times";

            // read response
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            Log.d(TAG, "DELETE Response -> " + conn);
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }

            in.close();

            Log.d(TAG, "DELETE Response -> " + response.toString());
            return response.toString();

        } catch (IOException e) {
            Log.d(TAG, "DELETE Response -> " + "here!!!! "+e.toString());
            return INTERNET_FAILED;
        }
    }
}