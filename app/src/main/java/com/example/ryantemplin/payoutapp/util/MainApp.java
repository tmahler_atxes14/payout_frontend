/**
 * Main Application class
 *
 * @author - Timothy Mahler
 * @version - 0.0.1
 */

package com.example.ryantemplin.payoutapp.util;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import com.example.ryantemplin.payoutapp.R;
import com.example.ryantemplin.payoutapp.activities.LoginActivity;
import com.example.ryantemplin.payoutapp.activities.RegisterUserActivity;
import com.example.ryantemplin.payoutapp.pojos.User;

public class MainApp extends Application {
    private static final String TAG = MainApp.class.getSimpleName();
    private boolean loggedIn = false;

    @Override
    public void onCreate() {
        super.onCreate();

        ImageUtil.initLoader(getApplicationContext());
    }

    /**
     * Get applications current version
     *
     * @return Application's version code from the Package Manger.
     */
    public static int getAppVersion(Context context) {
        try {
            if ((context != null) && (context.getPackageManager() != null)) {
                PackageInfo packageInfo = context.getPackageManager()
                        .getPackageInfo(context.getPackageName(), 0);
                return packageInfo.versionCode;
            }
            return Integer.MAX_VALUE;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            //Log.d(TAG, "Could not get package name: " + e);
            return Integer.MAX_VALUE;
        }
    }


    /**
     * Logs in current user
     *
     * @param user - User being logged in
     */
    public void logIn(User user) {
        loggedIn = true;
        Logger.d(TAG, "Logging in User\n"+user);

        // Store the user id to memory
        Memory.getInstance(this).storeUserId(user.getUserId());

        // Add new user to the datastore
        DataStore.getInstance().setUser(user);
    }

    /**
     * Logs currentUser out
     */
    public void logOut() {
        loggedIn = false;
        Logger.d(TAG, "Logging out User");

        // Clear temp userID
        Memory.getInstance(this).removeUserId();

        // Clear user from datastore
        DataStore.getInstance().setUser(null);

        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("com.package.ACTION_LOGOUT");
        sendBroadcast(broadcastIntent);
    }

}

