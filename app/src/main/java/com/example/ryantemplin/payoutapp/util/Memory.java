/**
 * Memory class handles all local storage
 *
 * @author Timothy Mahler
 * @version 0.0.1
 */

package com.example.ryantemplin.payoutapp.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;
import java.io.StreamCorruptedException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.ConcurrentModificationException;
import java.util.HashMap;

public class Memory {
    private static final String TAG = Memory.class.getSimpleName();

    public static String KEY_USER_ID = "userId";

    private static Context context;
    private static Memory instance;

    private Memory(Context context) {
        this.context = context.getApplicationContext();
    }

    public static Memory getInstance(Context context){
        if(instance == null){
            synchronized(DataStore.class){
                if(instance == null){
                    instance = new Memory(context);
                }
            }
        }
        return instance;
    }

    /**
     * Stores the given userId to memory
     *
     * @param id - id to store
     */
    public void storeUserId(int id) {
        SharedPreferences preferences =
                PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(KEY_USER_ID, id);
        editor.commit();
    }

    /**
     * Gets the userId from memory
     *
     * @return - userId if found, else return Integer.MIN_VALUE
     */
    public int getUserId() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        Integer userId = preferences.getInt(KEY_USER_ID, Integer.MIN_VALUE);

        if (userId == null)
            return Integer.MIN_VALUE; // default if no userId was set for some reason
        else
            return userId;
    }

    /**
     * Removes the userId from memory (called when logging out)
     *
     */
    public void removeUserId() {
        SharedPreferences preferences =
                PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(KEY_USER_ID);
        editor.commit();
    }

    /**
     * Store string value by key into shared preferences
     *
     * @param key        - key value
     * @param someString - string value
     * @param context
     */
    public static void storeSomeString(String key, String someString, Context context) {
        SharedPreferences preferences =
                PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, someString);
        editor.commit();
    }

    /**
     * Store int value by key into shared preferences
     *
     * @param key     - key value
     * @param num     - int value
     * @param context
     */
    public static void storeSomeInt(String key, int num, Context context) {
        SharedPreferences preferences =
                PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, num);
        editor.commit();
    }

    /**
     * Store int value by key into shared preferences
     *
     * @param key     - key value
     * @param num     - long value
     * @param context
     */
    public static void storeSomeLong(String key, long num, Context context) {
        SharedPreferences preferences =
                PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong(key, num);
        editor.commit();
    }

    /**
     * Get string value by key from shared preferences
     *
     * @param key     - key value
     * @param context - context
     * @return String value stored by key
     */
    public static String getSomeString(String key, Context context) {
        SharedPreferences preferences =
                PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key, null);
    }

    /**
     * Get int value by key from shared preferences
     *
     * @param key     - key value
     * @param context - context
     * @return int value stored by key
     */
    public static int getSomeInt(String key, Context context) {
        SharedPreferences preferences =
                PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getInt(key, 0);
    }

    /**
     * Get int value by key from shared preferences
     *
     * @param key     - key value
     * @param context - context
     * @return int value stored by key
     */
    public static long getSomeLong(String key, Context context) {
        SharedPreferences preferences =
                PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getLong(key, 0);
    }

    /**
     * Removes key,value from shared preferences
     *
     * @param key     - key value to be removed
     * @param context - context
     */
    public static void removeSomeKey(String key, Context context) {
        SharedPreferences preferences =
                PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(key);
        editor.commit();
    }

    /**
     * Stores any string "key"ed Hashmap to given local file
     *
     * @param arrayList - ArrayList being stored
     * @param file      - File where map_active is stored
     */
    public static <T> void storeArrayListToFile(ArrayList<T> arrayList, File file) {
        try {
            ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(file));
            outputStream.writeObject(arrayList);
            outputStream.flush();
            outputStream.close();
        } catch (ConcurrentModificationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Stores any string "key"ed Hashmap to given local file
     *
     * @param hm   - Hashmap being stored
     * @param file - File where map_active is stored
     * @param <T>  - generic type being hashed
     */
    public static <T> void storeHashToFile(HashMap<String, T> hm, File file) {
        try {
            //Log.d(TAG, "Writing hash to cash");
            ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(file));
            outputStream.writeObject(hm);
            outputStream.flush();
            outputStream.close();
        } catch (ConcurrentModificationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Reads generic type ArrayList from file.
     *
     * @param file - file to be read from
     * @param <T>  - generic type
     * @return hashamp read from file
     */
    public static <T> ArrayList<T> readArrayListFromFile(File file) {
        ArrayList<T> arrayList = new ArrayList<T>();
        try {
            FileInputStream fileStream = new FileInputStream(file);
            ObjectInputStream inputStream = new ObjectInputStream(fileStream);
            arrayList = (ArrayList<T>) inputStream.readObject();
            fileStream.close();
            inputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (OptionalDataException e) {
            e.printStackTrace();
        } catch (StreamCorruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ConcurrentModificationException e) {
            e.printStackTrace();
        } finally {
            return arrayList;
        }
    }

    /**
     * Reads generic type hashmap from given file
     *
     * @param type - obj hashed in map_active
     * @param file - file to be read from
     * @param <T>  - generic type
     * @return hashamp read from file
     */
    public static <T> HashMap<String, T> readHashFromFile(T type, File file) {
        HashMap<String, T> hashMap = null;
        try {
            FileInputStream fileStream = new FileInputStream(file);
            ObjectInputStream inputStream = new ObjectInputStream(fileStream);
            hashMap = (HashMap<String, T>) inputStream.readObject();
            fileStream.close();
            inputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (OptionalDataException e) {
            e.printStackTrace();
        } catch (StreamCorruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ConcurrentModificationException e) {
            e.printStackTrace();
        }
        return hashMap;
    }

    /**
     * Converts <string,Calendar> hashmap to <string,string>
     *
     * @param hm - hashamp being converted
     * @return - converted hashmap
     */
    public static HashMap<String, String> calendarToString(HashMap<String, Calendar> hm) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        HashMap<String, String> returnHash = new HashMap<String, String>();
        for (String c : hm.keySet()) {
            String dateString = formatter.format(hm.get(c).getTime());
            returnHash.put(c, dateString);
        }
        return returnHash;
    }

    /**
     * Converts <string,string> hashmap to <string,calendar>
     *
     * @param hm - hashamp being converted
     * @return - converted hashmap
     */
    public static HashMap<String, Calendar> stringToCalendar(HashMap<String, String> hm) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        HashMap<String, Calendar> returnHash = new HashMap<String, Calendar>();
        for (String c : hm.keySet()) {
            try {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(formatter.parse(hm.get(c)));
                returnHash.put(c, calendar);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return returnHash;
    }

}
