package com.example.ryantemplin.payoutapp.util;

import java.text.NumberFormat;

/**
 * Created by nickburrin on 4/22/15.
 */
public class MoneyUtil {

    public static String intToDollar(int amnt){
        double dollar = ((double) amnt)/100.00;
        NumberFormat formatter = NumberFormat.getCurrencyInstance();
        return formatter.format(dollar);
    }
}
