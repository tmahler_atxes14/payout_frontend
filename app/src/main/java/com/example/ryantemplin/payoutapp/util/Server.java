/**
 * Server class handles all server interaction
 *
 * @author Brain Wang & Timothy Mahler
 * @version 1.0     1/26/2014
 */

package com.example.ryantemplin.payoutapp.util;


public class Server {
    public static String URL_BASE = "http://timothyserver.cloudapp.net:80";
    public static String URL_USERS =  URL_BASE + "/users";
    public static String URL_REGISTER = URL_USERS + "/register";
    public static String URL_LOGIN = URL_USERS + "/login";
    public static String URL_GET_USER = URL_USERS +"/";
    public static String URL_DELETE_BETS = URL_USERS +"/:u_id/delete_bet/:g_id";
    public static String URL_CREATE_BETS = URL_USERS +"/:u_id/create_bet/:g_id";
    public static String URL_GET_GAMES = URL_BASE + "/games/todays_games";
    public static String URL_DELETE_USER = URL_USERS + "/delete_user/:u_id";
}
